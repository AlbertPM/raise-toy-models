// Gmsh project created on Wed Dec 22 20:54:51 2021
SetFactory("OpenCASCADE");
//
Rinf = DefineNumber[ 0.5, Name "Parameters/Rinf" ];
Rs = DefineNumber[ 0.02, Name "Parameters/Rs" ];
//
meshmult = 0.5;		// mesh refinement multiplication parameter (bigger = finer)

// Define 2 circles, one on the infinity boundary and other surrounding the surrogate area
// This is done to be able to define different mesh sizes on those lines

Circle(1) = {0, 0, 0, Rinf};
Circle(2) = {0, 0, 0, Rs};

// Create Curve loops from circles to be able to use them to get plane surfaces

Curve Loop (1) = {1};
Curve Loop (2) = {2};

//-------------------------------------------------------------------------
// Pattern creation or square surrogate substitution
//
lpatt = 0.01;            // ortogonal distance between elemnts in the matrix
rpatt = 0.002;           // radius of obstacle cylinders
//--- Variation for the pattern of obstacles start here
RectPattListLines[] ={};      // List of rectangular fins to export as boundaries
RectLooPattList[] = {}; // List of rectangular Loops for obstacles
Lsq = 0.012;		// half distance of obstacle area
hrect = 0.001;		// Height of heatsink fins

Point(3) = {-Lsq, -Lsq, 0};             Point(4) = {Lsq, -Lsq, 0};
Point(5) = {Lsq, -Lsq+hrect, 0};        Point(6) = {-Lsq, -Lsq+hrect, 0};
Line(3) = {3, 4}; Line(4) = {4, 5}; Line(5) = {5, 6}; Line(6) = {6, 3};
Curve Loop(3) = {3, 4, 5, 6};
RectPattListLines[] += {3 : 6};

Point(7) = {-Lsq, -(Lsq+hrect)/2, 0};           Point(8) = {Lsq, -hrect*8/17, 0};
Point(9) = {Lsq, hrect*8/17, 0};      Point(10) = {-Lsq, -(Lsq-hrect)/2, 0};
Line(7) = {7, 8}; Line(8) = {8, 9}; Line(9) = {9, 10}; Line(10) = {10, 7};
Curve Loop(4) = {7, 8, 9, 10};
RectPattListLines[] += {7 : 10};

Point(11) = {-Lsq, -hrect/Sqrt(3), 0};  Point(12) = {Lsq, Lsq-hrect/Sqrt(3), 0};
Point(13) = {Lsq-hrect, Lsq, 0};      Point(14) = {-Lsq, hrect/Sqrt(3), 0};
Line(11) = {11, 12}; Line(12) = {12, 13}; Line(13) = {13, 14}; Line(14) = {14, 11};
Curve Loop(5) = {11, 12, 13, 14};
RectPattListLines[] += {11 : 14};

Point(15) = {-Lsq, (Lsq-hrect*Sqrt(2))/2, 0};   Point(16) = {-(Lsq-hrect*Sqrt(2))/2, Lsq, 0};
Point(17) = {-(Lsq+hrect*Sqrt(2))/2, Lsq, 0};     Point(18) = {-Lsq, (Lsq+hrect*Sqrt(2))/2, 0};
Line(15) = {15, 16}; Line(16) = {16, 17}; Line(17) = {17, 18}; Line(18) = {18, 15};
Curve Loop(6) = {15, 16, 17, 18};
RectPattListLines[] += {15 : 18};

RectLooPattList[] += {3, 4, 5, 6};
//--- End of Variation
//-------------------------------------------------------------------------
// Create the plane surfaces (Same lines can be used for both cases

Plane Surface(1) = {1, 2};
Plane Surface(2) = {2, RectLooPattList[]};
Delete {Surface{3 : 6};}

// Using mesh size applied to the points of the circles
MeshSize{ PointsOf{ Curve{1}; } }                   	= 0.02  / meshmult;
MeshSize{ PointsOf{ Curve{2}; } }                   	= 0.001 / meshmult;
// Use this line in case of using the matrix pattern...
MeshSize{ PointsOf{ Curve{RectPattListLines[]}; } }     = 0.0002/ meshmult;

// Physical entities definition
Physical Line(     "Infty_B")   	= {1};
Physical Line(     "Obst_B")    	= {RectPattListLines[]};
Physical Surface(   "OutDomain")        = {1};
Physical Surface(   "InDomain")         = {2};

