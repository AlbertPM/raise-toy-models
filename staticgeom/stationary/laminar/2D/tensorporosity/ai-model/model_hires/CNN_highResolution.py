# import the necessary packages
from torch import flatten
from torch.nn import    Module, Conv2d, Linear, MaxPool2d, \
                        LeakyReLU, AdaptiveMaxPool2d, BatchNorm1d, ReLU, \
                        Hardtanh


def _init_weights(module):
    if isinstance(module, Linear):
        module.weight.data.normal_(mean=0.0, std=1.0)
        if module.bias is not None:
            module.bias.data.zero_()
    elif isinstance(module, Conv2d):
        module.weight.data.normal_(mean=0.5, std=0.1)
        if module.bias is not None:
            module.bias.data.zero_()


class ConvolutionalNetwork_HR(Module):

    def __init__(self, num_channels, p_min, p_max):
        # call the parent constructor
        super(ConvolutionalNetwork_HR, self).__init__()

        self.activConv = LeakyReLU()
        self.max_pool = MaxPool2d(kernel_size=(2, 2), stride=(2, 2))
        self.activFC = LeakyReLU()
        self.activOUT = Hardtanh(min_val=0.0, max_val=1.0)

        # initialize first set of CONV => RELU => POOL layers
        self.conv1a = Conv2d(in_channels=num_channels, out_channels=15,
                             kernel_size=(5, 5), stride=(1, 1), padding=(4, 4))


        # initialize second set of CONV (=> RELU => POOL layers)
        self.conv2a = Conv2d(in_channels=15, out_channels=30,
                             kernel_size=(5, 5), stride=(1, 1), padding=(4, 4))


        # initialize third set of CONV (=> RELU => POOL layers)
        self.conv3a = Conv2d(in_channels=30, out_channels=45,
                             kernel_size=(3, 3), stride=(1, 1), padding=(2, 2))



        # initialize third set of CONV (=> RELU => POOL layers)
        self.conv4a = Conv2d(in_channels=45, out_channels=60,
                             kernel_size=(3, 3), stride=(1, 1), padding=(2, 2))


        # initialize separate set of FC => RELU layers
        self.fca = Linear(in_features=4200, out_features=800)

        # initialize first common linear layer FC => RELU
        self.fc1 = Linear(in_features=800, out_features=200)


        self.bn1 = BatchNorm1d(num_features=800)
        
        # get output layer as a single value
        self.fcOut = Linear(in_features=200, out_features=4)

        # self.apply(self._init_weights)
        
        self.pmin = p_min
        self.pmax = p_max

    def forward(self, xa):
        # pass the input through our first set of CONV => RELU =>
        # POOL layers
        # IN:  xa == (76 x 126)x2    xb == (126 x 126)x2
        # OUT: xa == (40 x 65)x15   xb == (65 x 65)x15
        # print(f"xa1: {xa.size()}")
        xa = self.conv1a(xa)  # 5x5 kernel, 1x1 stride
        xa = self.activConv(xa)
        xa = self.max_pool(xa)

        # pass the output from the previous layer through the second
        # set of CONV => RELU => POOL layers
        # IN:  xa == (40 x 65)x15   xb == (65 x 65)x15
        # OUT: xa == (22 x 35)x30     xb == (35 x 35)x30
        # print(f"xa2: {xa.size()}")
        xa = self.conv2a(xa)  # 5x5 kernel, 1x1 stride
        xa = self.activConv(xa)
        xa = self.max_pool(xa)  # 2x2 pooling

        # pass the output from the previous layer through the second
        # set of CONV => RELU => POOL layers
        # IN:  xa == (22 x 34)x30   xb == (34 x 34)x30
        # OUT: xa == (12 x 18)x45     xb == (18 x 18)x45
        # print(f"xa3: {xa.size()}")
        xa = self.conv3a(xa)  # 3x3 kernel, 1x1 stride
        xa = self.activConv(xa)
        xa = self.max_pool(xa)  # 2x2 pooling

        # pass the output from the previous layer through the second
        # set of CONV => RELU => POOL layers
        # IN:  xa == (12 x 18)x45   xb == (18 x 18)x45
        # OUT: xa == (7 x 10)x60     xb == (10 x 10)x60
        # print(f"xa4: {xa.size()}")
        xa = self.conv4a(xa)  # 3x3 kernel, 1x1 stride
        xa = self.activConv(xa)
        xa = self.max_pool(xa)  # 2x2 pooling

        # flatten the output from the previous layer
        # IN:  xa == (7 x 11)x60     xb == (11 x 11)x60
        # OUT: xa == 4620           xb == 7260
        # print(xa.size(), xb.size())
        # print(f"xa5: {xa.size()}")
        # print(f"xb5: {xb.size()}")
        xa = flatten(xa, 1)  # flatten on dim 1 to account for batch dim 0

        # pass the output through 2 separate sets of FC => RELU layers
        # IN:  xa == 4200           xb == 6000
        # OUT: xa == 800            xb == 1600
        xa = self.fca(xa)
        xa = self.activFC(xa)


        # Concatenate the 2 tensors into one and pass them through
        # 2 set of set of FC => RELU layers
        # IN:  xa == 800        xb == 1600
        # OUT: x == 300
        x = self.fc1(xa)
        x = self.bn1(x)
        x = self.activFC(x)

        # pass the output to final layer to get our output predictions
        # IN:  x == 300
        # OUT: x == 4
        x = self.fcOut(x)
        output = self.activOUT(x)

        # return the output regression
        return output

    def real_output(self, x_a):
        pnorm = self.forward(x_a)
        for pn in pnorm:
            for i,p in enumerate(pn):
                pn[i] = p * (self.pmax[i] - self.pmin[i]) + self.pmin[i]
        return pnorm