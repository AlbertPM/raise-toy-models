#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 17:29:39 2024

@author: al
"""

import pickle
import torch
import numpy as np
import matplotlib.pyplot as plt

from common.alya_dataset import AlyaDataset


"""
Class definition to store summarize datasets
"""

class dataSummary():
    def __init__(self, dataset):
        self.vIn = []
        for i, v in enumerate(dataset.vIn):
            a = dataset.ang[i]
            self.vIn.append([v*np.cos(np.pi * a / 180), v*np.sin(np.pi * a / 180)])
        self.vUp = []
        for v in dataset.x1_data:
            vx = float(torch.mean(v[0]))
            vy = float(torch.mean(v[1]))
            self.vUp.append([vx, vy])
        self.vDw = []
        for v in dataset.x2_data:
            vx = float(torch.mean(v[0]))
            vy = float(torch.mean(v[1]))
            self.vDw.append([vx, vy])
        self.pMax = dataset.pMax
        self.pMin = dataset.pMin
        self.poro = []
        for p in dataset.y_data:
            self.poro.append(list(p))

"""
Function to read the information from the datasets
"""

def get_Summarized_Data(datasetName):
    print("Load old dataset --- Start")
    with open(datasetName, 'rb') as f:
        train_dataset = pickle.load(f)
        print("train read")
        _ = pickle.load(f)
        print("test read")
    print("Load old dataset --- Done")
    return dataSummary(train_dataset)

"""
Plot functions
"""

def plotData(data):
    
    

#%%

"""
Analysis of original dataset
"""

trainData1 = get_Summarized_Data("surrDS-te_amd.bin")

#%%


print(trainData1.vIn)

#%%

"""
Analysis of new dataset
"""

trainData2 = get_Summarized_Data("surrDS-te2_amd.bin")

print(test_dataset2.vIn[0])
print(test_dataset2.ang[0])
print(test_dataset2.pMax, test_dataset2.pMin)
print(test_dataset2.x1_data.size())
print(test_dataset2.x1_data[0][0])
print(test_dataset2.x2_data.size())
print(test_dataset2.x2_data[0][0])

