#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 17 19:00:31 2023

@author: al
"""

import sys
import subprocess
import pickle

from os import listdir
from os.path import exists,isfile, join



"""
Functions to look for available folders and processes to perfom operations
with them
"""


# Check partial completion status in 'results' folder
def update_completion_folder(comm, caseFolder, foldList, nSim_Folder, 
                             notCompleted, foldLocked):
    # Check for each different folder to run simulations in...    
    for i, val in enumerate(foldList):
        # ...that already completed simulations are equal to total needed
        if ( nSim_Folder == val ):
            notCompleted[i] = False   
            foldLocked[i] = True
        elif ( nSim_Folder < val):
            print(f"[ERROR]: Cases for {caseFolder}-{i} > number of runs",
                  file=sys.stderr)
            comm.Abort()

            
# Check partial completion status based on 'results' folder
def check_completion_folder_num_error(cellList, nSim_Cell):
    # Check for each different folder to run simulations in...    
    for i, val in enumerate(cellList):
        # ...that there are not more simulations than required per Cell
        if ( val > nSim_Cell ):
            return i
    return -1

            
# Return number of first idle process available or 0 for manager process
def find_idle_process(pList):
    # List idle status '1' in pList
    iMasked = [i for i,st in enumerate(pList) if st == 1 ]
    
    # Return last available index (manager = 0 in case all processes are busy)
    return iMasked[-1]


# Find a folder index to look for porosity limit value.
def find_folder_to_poro_RMA(pLFoundL, lockL):
    # get masked list depending on whether the porosity limit has been found
    iMasked = [i for i,stat in enumerate(pLFoundL) if (not lockL[i] and not stat)]
    
    # return first index of folder with porosity yet not found
    # or -1 if they are all locked or already found
    if len(iMasked) > 0:
        idx = iMasked[0]
    else:
        idx = -1
        
    return idx


# Find a folder index to look for porosity limit value.
def find_folder_to_poro_P2P(poroS):
    # get masked list depending on whether the porosity limit has been found
    iMasked = [i for i,stat in enumerate(poroS) if (stat == 0)]
    
    # return first index of folder with porosity yet not found
    # or -1 if they are all locked or already found
    if len(iMasked) > 0:
        idx = iMasked[0]
    else:
        idx = -1
        
    return idx
    

# Find the folder index with most pending runs
def find_folder_to_sim(foldL, pLFoundL, lockL):
    # get masked list depending on completion status of runs in folder
    iMasked = [i for i,f in enumerate(foldL) if not lockL[i] and pLFoundL[i]]
    fMasked = [f for i,f in enumerate(foldL) if not lockL[i] and pLFoundL[i]]
    
    # return first index of folder with most pending cases
    # or -1 if all are locked
    if len(fMasked) > 0:
        # Look for min value in fMasked
        minV = min(fMasked)
        # find index of min value in fMasked
        idxMin = fMasked.index(minV)
        # return index on unmasked list
        idx = iMasked[idxMin]
    else:
        idx = -1
    
    return idx


# return number of simulated cases from 'nFold' folder
def count_simulated_cases(rFold, nFold):
    # In case 'results' folder doesn't exist yet
    if (not exists(rFold)):
        return 0
    else:
        # Get a list with all files in 'results folder'
        fileList = [f for f in listdir(rFold) if isfile(join(rFold, f))]
        # Set counter of files from folder 'nFold'
        count = 0                             
        for f in fileList:
            nameSplit = f.split('-')        # Split the name of the file
            #print(f"{nameSplit}")
            if ( f"{nameSplit[0]}-{nameSplit[1]}" == nFold and
                 nameSplit[-1][-3:] == "txt"): count += 1
        return count
    
    
# delete the temporary case folders  
def rm_temp_folders(tempF, fold, nPr):
    for fName in [f"{tempF}/{fold}-{i}" for i in range(1, nPr)]:
        subprocess.run(["rm", "-r", fName])
        
        
# Check whether porosity limits saved file exists
def exists_saved_porosities(pFile):
    return exists(pFile)
        

# Read stored porosity limits from file
def read_porosity_limits(nVpro, pFile):
    with open(pFile, 'rb') as f:
        nVf = pickle.load(f)
        pL = pickle.load(f)
    if nVpro == nVf:
        return pL
    else:
        exit("[ERROR]: Loading porosity file failed ")

    
# Write porosity limits found to a file
def save_porosity_limits(nV, pLims, pFile):
    with open(pFile, 'wb') as f:
        pickle.dump(nV, f, pickle.HIGHEST_PROTOCOL)
        pickle.dump(pLims, f, pickle.HIGHEST_PROTOCOL)

 
    