#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 16:52:32 2024

@author: al
"""

import numpy as np

from parallel_porosity import get_random_porosity, get_random_porosity_list

import modules.parallel_params as pp


# Returns the limits used to get a random angle and velocity for 
# each folder
def get_limits_for_cell(aLims, vLims, nCell):
    nV = len(vLims) - 1
    
    # Folders will be split first on angles and then on velocity
    aIdx = nCell // nV
    vIdx = nCell % nV
    
    return aLims[aIdx:aIdx+2], vLims[vIdx:vIdx+2]



# Obtain random values for angle and modulus of velocity and surrogate
# porosity for the  
def get_vars_conti(nSF, aL, vL, pL, rng, nC, hR):    
    # get random uniform distributed values of angles and velocity inside
    # local limits (global velocity is not uniform)
    ang = rng.uniform(aL[0], aL[1], nSF)[nC-1]
    vel = rng.uniform(vL[0], vL[1], nSF)[nC-1]
    # get porosity value from calculated limits for that folder based
    # modify porosity to 0 if high resolution cases are simulated
    if hR:
        por =[0,0,0,0]
    else:
        por = get_random_porosity(nSF, pL, rng, nC)
    
    return (ang, vel, por)


        
def generate_poro_values(simValList, nSim_Cell, vIdx, pList):
    # Set random seed based on pSrchIdx
    seed = int.from_bytes(f"{vIdx}".encode(), 'big')
    rng = np.random.default_rng(seed)
    # Read poro limits for velocity index
    pLims = pList[2*vIdx:2*vIdx+2]
    # Fill simValList for all angle indices at given velocity index
    for aIdx in range(pp.nAbins):
        # Get poro distribution array (depends on distribution type defined)
        pValues = get_random_porosity_list(nSim_Cell, pLims, rng)
        for i in range(nSim_Cell):
            simValList[nSim_Cell * (aIdx * pp.nVbins + vIdx) + i][2] = pValues[i]   
            
            
def AV2C(a, v):
    return a * pp.nVbins + v

def C2AV(c):
    return (c // pp.nVbins, c % pp.nVbins)