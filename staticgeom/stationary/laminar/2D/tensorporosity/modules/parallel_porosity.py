#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 17:38:31 2023

@author: al
"""

import sys
import numpy as np
import sympy as sp
import os.path


file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)


import parallel_params as pp


# Returns the maximum value for porosity range depending on simulation
# nominal velocity, fluid parameters and surrogate characteristic length
def get_theory_poro_limits(v):
    advO = v / pp.obstLen
    visO = pp.visc / pp.obstLen**2
    # set max range for porosity as 1e4 times the max contributor on N-S eq.
    return [min(advO, visO)/pp.multPorMax, pp.multPorMax * max(advO, visO)]


# Select the porosity distribution and return N random values from it between
# the limits defined in pL
def poro_dist(nSF, rng, N, dType, pL):
    pmin, pmax = (pL[0], pL[1])
    if (dType == 'uniform'):        # Uniform between pmin and pmax
        pDist = rng.uniform(pmin, pmax, N * nSF)
    elif (dType == 'triangular'):   # Triangular between pmin and pmax
        pDist = rng.triangular(pmin, pmin, pmax, N * nSF)
    elif (dType == 'log'):          # Logaritmic between pmin and pmax
        plog = rng.uniform(np.log(pmin), np.log(pmax), N * nSF)
        pDist = np.exp(plog)
    else:
        print("[ERROR]: parallel_porosity.py/poro_dist ->"+
              "Bad porosity distribution defined")
        exit()
    return pDist


# Return a porosity matrix from a diagonal p1, p2 porosity matrix
# on a theta rotated basis
def get_rotated_matrix(p1, p2, theta):
    cosTh = np.cos(theta)
    sinTh = np.sin(theta)
    D1 = p1 * cosTh**2 + p2 * sinTh**2
    D2 = p1 * sinTh**2 + p2 * cosTh**2
    S  = (p1 - p2) * cosTh * sinTh
    return [D1, S, S, D2] 


# Get a matrix in canonical basis from a diagonal matrix in an
# eigenvector space corresponding to 
def get_changed_basis(p1, p2, theta1, theta2):
    # Get normalized eigenvectors expressed in normal basis
    eigenV = [ np.cos(theta1), np.sin(theta1) ]
    eigenW = [ np.cos(theta2), np.sin(theta2) ]
    # Get change of basis matrix from eigenvector basis to canonical
    S = sp.Matrix( [ [eigenV[0], eigenW[0]], [eigenV[1], eigenW[1]] ])
    # Get diagonal matrix in eigenvector basis
    D = sp.Matrix( [ [p1, 0], [0, p2]] )
    # Transform to canonical space
    M = S * D * S.inv
    return [ float(m) for m in [M[0,0], M[1,0], M[0,1], M[1,1]] ]


def get_random_porosity(nSC, pL, rng, nC):
    # Calculate porosity from random distributions as an scalar value or
    # 2 different creation process tensors
    if ( pp.poroType == 'scalar' ):
        idx = (nC-1)*1
        por = float( poro_dist(nSC, rng, 1, pp.poroDistrib, pL)[idx:idx+2] )
    elif ( pp.poroType == 'rotBasis' ):
        idx = (nC-1)*2
        # get 2 porosity ortogonal eigenvalues and 1 rotation angle
        por = list( poro_dist(nSC, rng, 2, pp.poroDistrib, pL)[idx:idx+3] )
        th = rng.uniform(-np.pi/2, np.pi/2, nSC)[nC-1]
        # Calculate rotated porosity matrix
        por = get_rotated_matrix(por[0], por[1], th)
    elif ( pp.poroType == 'changeBasis' ):
        # get 2 porosity ortogonal eigenvalues and
        # 2 eigenvector directions
        idx = (nC-1)*2
        por = list( poro_dist(nSC, rng, 2, pp.poroDistrib, pL)[idx:idx+3] )
        th1 = rng.uniform(-np.pi/2, np.pi/2, nSC)[nC-1]
        th2 = rng.uniform(-np.pi/2, np.pi/2, nSC)[nC-1]
        # Calculate the change of basis
        por = get_changed_basis(por[0], por[1], th1, th2)
    else:
        print("[ERROR]: parallel_porosity.py/get_random_porosity ->"+
              "Bad porosity type defined")
        exit()
    
    return por


def get_constant_Txx(p, th, pL):
    pmin = pL[0]
    pmax = pL[1]
    # Convert porosity ranges in fractions between poro min and max
    pfrac = (p - pmin) / (pmax - pmin)
    # Define xx component of tensor as 90% between poro min and max
    Txx = pmin + pp.TxxFraction * (pmax - pmin)
    c = np.cos(th)
    s = np.sin(th)
    # a1 = Txx / c**2 - s**2 / c**2 * a2
    if ( c**2 > s**2 ):
        a2_min = pmin
        a2_max = pmax
        # check inside upper limit for a1(a2)
        if ( (Txx - s**2 * pmin)/c**2 > pmax ):
            a2_min = (Txx - pmax * c**2)/s**2
        if ( (Txx - s**2 * pmax)/c**2 < pmin ):
            a2_max = (Txx - pmin * c**2)/s**2            
        a2 = a2_min + pfrac * (a2_max - a2_min)
        a1 = (Txx - a2 * s**2)/c**2
    # a2 = Txx / s**2 - c**2 / s**2 * a1
    else:
        a1_min = pmin
        a1_max = pmax
        # check inside upper limit for a2(a1)
        if ( (Txx - c**2 * pmin)/s**2 > pmax ):
            a1_min = (Txx - pmax * s**2)/c**2
        if ( (Txx - c**2 * pmax)/s**2 < pmin ):
            a1_max = (Txx - pmin * s**2)/c**2
        a1 = a1_min + pfrac * (a1_max - a1_min)
        a2 = (Txx - a1 * c**2)/s**2
    Txy = (a1 - a2) * c * s
    Tyy = a1 * s**2 + a2 * c**2
    return [ Txx, Txy, Txy, Tyy ]


# Modification for new P2P communication scheme
# All porosities are calculated for all the cases that share the same limits
def get_random_porosity_list(nSC, pL, rng):
    # Calculate porosity from random distributions as an scalar value or
    # 2 different creation process tensors
    if ( pp.poroType == 'scalar' ):
        pDist = poro_dist(nSC, rng, 1, pp.poroDistrib, pL)
        por = [ float(pDist[i]) for i in range(nSC) ]
    elif ( pp.poroType == 'constantTxx'):
        pDist = poro_dist(nSC, rng, 1, pp.poroDistrib, pL)
        th = rng.uniform(-np.pi/2, np.pi/2, nSC)
        por = [ get_constant_Txx(pDist[i], th[i], pL) for i in range(nSC) ]
    elif ( pp.poroType == 'rotBasis' ):
        # get lists of 2 porosity ortogonal eigenvalues and 1 rotation angle
        pDist = poro_dist(nSC, rng, 2, pp.poroDistrib, pL)
        th = rng.uniform(-np.pi/2, np.pi/2, nSC)
        # Calculate rotated porosity matrix
        por = [ get_rotated_matrix(pDist[2*i], pDist[2*i+1] , th[i]) 
                for i in range(nSC) ]
    elif ( pp.poroType == 'changeBasis' ):
        # get 2 porosity ortogonal eigenvalues and
        # 2 eigenvector directions
        pDist = poro_dist(nSC, rng, 2, pp.poroDistrib, pL)
        th1 = rng.uniform(-np.pi/2, np.pi/2, nSC)
        th2 = rng.uniform(-np.pi/2, np.pi/2, nSC)
        por = [ get_changed_basis(pDist[2*i], pDist[2*i+1], th1[i], th2[i]) 
                for i in range(nSC)]
    else:
        print("[ERROR]: parallel_porosity.py/get_random_porosity ->"+
              "Bad porosity type defined")
        exit()
    
    return por



    