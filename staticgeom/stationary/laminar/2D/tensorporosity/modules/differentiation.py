#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 25 16:18:19 2023

@author: al
"""

import math
import numpy as np


def derivatives(xarr, yarr):
    N = len(xarr)
    darr = []
    ddarr = []
    # Loop that looks for derivatives at each available point i
    for i in range(N):
        b = []
        M = []
        # Loop that files the rows of the system to solve 
        for j in range(N):
            if j != i:
                b.append(yarr[j] - yarr[i])
                # We look only for derivatives using cuadratic aproximations
                # at most
                Mrow = [(xarr[j] - xarr[i])**(k+1) / math.factorial(k+1) 
                        for k in range(min(N-1, 2))]
                M.append(Mrow)
        b = np.array(b)
        M = np.array(M)
        # Cuadratic function is approximated for an arbitrary number of
        # point pairs using least squares
        X = np.linalg.lstsq(M, b, rcond=None)[0]
        darr.append(X[0])
        ddarr.append(X[1])
    return [darr, ddarr]