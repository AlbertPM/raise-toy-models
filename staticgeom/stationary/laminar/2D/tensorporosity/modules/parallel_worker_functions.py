#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 19 15:26:54 2023

@author: al
"""


import subprocess
from os.path import exists
import numpy as np
from scipy.stats import norm
from scipy.optimize import curve_fit
from scipy.special import ndtri
import time

import parallel_params as pp


from mpi_datatypes import mpi_dtype_float, np_dtype_float, np_dtype_int

from alya_simulate import run_case, clean_outputs, get_mesh, \
                          get_downstream_V_field

from alya_processing import check_alya_convergence

from parallel_porosity import get_random_porosity, get_theory_poro_limits

from parallel_common_functions import get_limits_for_cell, get_vars_conti


# Create a numpy random generator using as seed the text on "string"
# Used to guarantee repeatability on the simulations
def set_seed_for_cell(string):
    seed = int.from_bytes(f"{string}".encode(), 'big')
    return np.random.default_rng(seed)



# create a copy of the folder to run the simulation there in parallel
# without interfering with other processes
def create_temp_folder(fold, newFold):
    if (not exists(newFold)):
        subprocess.run(["cp", "-r", f"./{fold}", newFold])


# Returns (TRUE, idx) if there are still porosities to be found (-1) in poroStatus
# and the first occurrence is idx and (FALSE, maxIdx+1) otherwise.
def check_pending_poro_limits(pStatus):
    if (pStatus == -1).any():
        pendingPoros = True
        idxPoro = np.argmax(pStatus == -1)
    else:
        pendingPoros = False
        idxPoro = len(pStatus)
    return (pendingPoros, idxPoro)


# Return the porosity value limits for the folder with index idxFolder
def get_poro_lim_values(win, idxFolder):
    buf = np.zeros(2, dtype=np_dtype_float)
    win.Lock(rank=0)
    win.Get(buf, target_rank=0, target=(2*idxFolder, 2, mpi_dtype_float))
    win.Unlock(rank=0)
    return [float(buf[0]), float(buf[1])]


# Run a case in a folder using the continuation feature from Alya to start
# from a previously solved simulation.
# nSF       -> Number of simulations per folder
# tempF     -> Pathname of the temporal folder where simulations will run
# fold      -> Name of the folder with data for the simulation
# case      -> Base name of the files with data for the simulation
# nF        -> Index of the folder where a parallel simulation is run
# pL        -> Array with lower and upper limits of porosity for that folder
# nC        -> Number of simulation being run for that folder
# hR        -> Flag to define whether the simulation is for a high Res model
def run_conti_case(nSF, tempF, fold, case, nCe, nPr, pL, nC, hR = False):
    # create a copy of the folder to run the simulation there in parallel
    # without interfering with other processes (in case it doesn't exist yet)
    newFold = f"{tempF}/{fold}-{nPr}"
    create_temp_folder(fold, newFold)
    
    # Set random seed for that particular cell
    rng = set_seed_for_cell(f"{fold}-{nCe}")
    
    # Get random angle, velocity and porosity
    #   get angle and velocity limits for the used folder idx
    aBin, vBin = get_limits_for_cell(pp.aBinsLims, pp.vBinsLims, nCe)
    #   get particular values for angle, velocity and porosities
    a, v, p = get_vars_conti(nSF, aBin, vBin, pL, rng, nC, hR)
    
    # run the case with ang and vel values
    run_case(newFold, case, a, v, p, False, HPC=True, tSteps=pp.timeSteps)

    cvgFile = f"{newFold}/{case}.nsi.cvg"
    if (not check_alya_convergence(cvgFile, v)):
        svF = f"./results/{fold}-{nCe}-{a}-{v}-{p[0]}x{p[1]}x{p[2]}x{p[3]}.cvg"
        subprocess.run(["cp", cvgFile, svF])
    
    # clean output from simulation for the next one but keep necessary data
    #   for restarting the simulation with the previous values
    clean_outputs(newFold, case)


# Modification for P2P comm with variables passed
def run_conti_case_P2P(tempF, fold, case, nCe, nPr, sData, hR = False):
    # create a copy of the folder to run the simulation there in parallel
    # without interfering with other processes (in case it doesn't exist yet)
    newFold = f"{tempF}/{fold}-{nPr}"
    create_temp_folder(fold, newFold)
    
    # get values for angle, velocity and porosities
    a = sData[0]
    v = sData[1]
    p = sData[2]
    
    # run the case with ang and vel values
    run_case(newFold, case, nCe, a, v, p, False, HPC=True, tSteps=pp.timeSteps)

    cvgFile = f"{newFold}/{case}.nsi.cvg"
    if (not check_alya_convergence(cvgFile, v)):
        svF = f"./results/{fold}-{nCe}-{a}-{v}-{p[0]}x{p[1]}x{p[2]}x{p[3]}.cvg"
        subprocess.run(["cp", cvgFile, svF])
    
    # clean output from simulation for the next one but keep necessary data
    #   for restarting the simulation with the previous values
    clean_outputs(newFold, case)


# Get the average of L2 diff between a vector fields and a reference constant
# vector field with velocity mod v and argument a
def field_diff(VF, v, a):
    arad = a * np.pi / 180
    VRef = np.array([v * np.cos(arad), v * np.sin(arad)]) * np.ones_like(VF)
    Vdif2 = (VF - VRef)**2
    Vdifnorm = np.sqrt(np.sum(Vdif2, 2))
    return np.average(Vdifnorm) # / np.average(V0norm)


    
# Find the maximum porosity to simulate at each folder conditions (v and ang)
# tempF     -> Pathname of the temporal folder where simulations will run
# fold      -> Name of the folder with data for the simulation
# case      -> Base name of the files with data for the simulation
# nPr       -> Number of process to create the Folder for parallel simulations
# nC        -> Index of the simulation variables cell conditions
def run_find_max_poro(tempF, fold, case, nPr, nC):
    
    def propose_new_limits(xarr, yarr, xsol0):
        # Try to fit points with a normal cdf
        fNorm = lambda x_f,mu,sigma, M: M*norm(mu,sigma).cdf(x_f)
        mu, sigma, M = curve_fit(fNorm, xarr, yarr)[0]       
        # Get the points where CDF is 0.01 and 0.99
        xl = ndtri(0.005) * sigma + mu
        xr = ndtri(0.995) * sigma + mu
        
        return np.array([xl, xr])

    def adjust_new_points(pLN, lP):
        N = len(lp)
        pNew = []
        for pL in pLN:
            idx = np.searchsorted(lP, pL)
            if idx == 0:
                pNew.append(1.5*lP[0]-0.5*lP[1])
            elif idx == N:
                pNew.append(1.5*lP[N-1]-0.5*lP[N-2])
            else:
                pNew.append(0.5*(lP[idx]+lP[idx-1]))
        
        return np.array(pNew)
    
    # create a copy of the folder to run the simulation there in parallel
    # without interfering with other processes
    newFold = f"{tempF}/{fold}-{nPr}"
    create_temp_folder(fold, newFold)

    # get the mesh related files without output on stdout
    get_mesh(newFold, case, False)  
    
    # Get angle and velocity
    #   get angle and velocity limits for the used cell idx
    #   and set the average angle and velocity for that cell
    aBin, vBin = get_limits_for_cell(pp.aBinsLims, pp.vBinsLims, nC)
    a = sum(aBin) / 2
    v = sum(vBin) / 2
    
    # Initially defined porosity limits based on theoretical order of magnitude
    ptheo = get_theory_poro_limits(v)    

    # Pass to log and expand for derivatives search
    lp = np.log10(ptheo)
    varlp = lp[1]-lp[0]
    lp = np.array([lp[0]-varlp/6, lp[0], lp[0]+varlp/3,
                   lp[1]-varlp/3, lp[1], lp[1]+varlp/6])
    
    # Index of starting proposed porosity limits
    pIdx = [1, 4]
    
    # Simulate and obtain normalized velocity field average variation (NVFAV)
    DV = []         # List of NVFAV values
    # Populate DV list
    for p in list(10**lp):
        # Get downstream velocity field for a, v and p conditions
        VF = get_downstream_V_field(newFold, case, a, v, float(p))
        # Add NVFAV value against homogeneous field with v and a
        DV.append(field_diff(VF, v, a))
    DV = np.array(DV)
    
    pLim = np.array([lp[pIdx[0]], lp[pIdx[1]]])
    # Flag for porosity found
    notPoroLimitsFound = True                                   
    while notPoroLimitsFound:
        # Propose new points as the porosity limits
        pLimOld = pLim
        pLim = propose_new_limits(lp, DV, pLimOld)
        # Check convergence
        pRange0 = pLimOld[1] - pLimOld[0]
        pRange = pLim[1] - pLim[0]
        # DEBUG INFO -----
        print(f"v = {v} | {len(lp)} | {lp}")
        print(f"v = {v} | porL = {pLim[0]}, {pLim[1]}")
        print(f"v = {v} | old Range = {pRange0}, new Range = {pRange}, "+
              f"diff = {pRange - pRange0}")
        # -----
        if ( abs(pRange - pRange0) < 1e-2 * max(pRange, pRange0) or
             len(lp) >= 10 ):
            notPoroLimitsFound = False
            next
        # find 2 new points to simulate
        pNew = adjust_new_points(pLim, lp)
        # Simulate for new points NVFAV
        newDV = []
        for p in list(10**pNew):
            VF = get_downstream_V_field(newFold, case, a, v, float(p))
            newDV.append(field_diff(VF, v, a))
        newDV = np.array(newDV)
        # Insert new points in existing arrays
        insertIdx = np.searchsorted(lp, pNew)
        lp = np.insert(lp, insertIdx, pNew)
        DV = np.insert(DV, insertIdx, newDV)
    
    return float(10**pLim[0]), float(10**pLim[1])


