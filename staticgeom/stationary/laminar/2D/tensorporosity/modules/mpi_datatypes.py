#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 18:14:44 2023

@author: al
"""

from mpi4py import MPI
from mpi4py.util import dtlib


mpi_dtype_int = MPI.INT
mpi_size_int = mpi_dtype_int.Get_size()
np_dtype_int = dtlib.to_numpy_dtype(mpi_dtype_int)

mpi_dtype_float = MPI.FLOAT
mpi_size_float = mpi_dtype_float.Get_size()
np_dtype_float = dtlib.to_numpy_dtype(mpi_dtype_float)

mpi_dtype_bool = MPI.BOOL
mpi_size_bool = mpi_dtype_bool.Get_size()
np_dtype_bool = dtlib.to_numpy_dtype(mpi_dtype_bool)
