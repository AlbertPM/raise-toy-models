// Gmsh project created on Wed Dec 22 20:54:51 2021
SetFactory("OpenCASCADE");
//
Rinf = DefineNumber[ 0.5, Name "Parameters/Rinf" ];
Rs = DefineNumber[ 0.02, Name "Parameters/Rs" ];
//
meshmult = 0.5;		// mesh refinement multiplication parameter (bigger = finer)

// Define 2 circles, one on the infinity boundary and other surrounding the surrogate area
// This is done to be able to define different mesh sizes on those lines

Circle(1) = {0, 0, 0, Rinf};            Curve Loop (1) = {1};
Circle(2) = {0, 0, 0, Rs};              Curve Loop (2) = {2};

//-------------------------------------------------------------------------
// Pattern creation or square surrogate substitution
//
lpatt = 0.01;           // ortogonal distance between elemnts in the matrix
rpatt = 0.002;          // radius of obstacle cylinders
// Surrogate square enclosing area for the 3x3 matrix pattern
// from (- lpatt - rpatt, - lpatt - rpatt) to (lpatt + rpatt, lpatt + rpatt)
//--- Variation for the surrogate model
VerXY = lpatt + rpatt;   // Square vertices are symmetrical from origin
Rectangle(3) = {-VerXY, -VerXY, 0, 2*VerXY, 2*VerXY};
CirPattList[] = {3, 4, 5, 6};
//--- End of Variation
//-------------------------------------------------------------------------

// Create the plane surfaces (Same lines can be used for both cases

Plane Surface(1) = {1, 2};
Plane Surface(2) = {2, 3};

// Using mesh size applied to the points of the circles
MeshSize{ PointsOf{ Curve{1}; } }                   = 0.02  / meshmult;
MeshSize{ PointsOf{ Curve{2}; } }                   = 0.001 / meshmult;
MeshSize{ PointsOf{ Curve{CirPattList[]}; } }                   = 0.001 / meshmult;


// Physical entities definition
Physical Line(     "Infty_B")   	= {1};
Physical Surface(   "OutDomain")        = {1};
Physical Surface(   "InDomain")         = {2};
Physical Surface(   "Surrogate")        = {3};
