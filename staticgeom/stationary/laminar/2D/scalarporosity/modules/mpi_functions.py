#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 17:44:29 2023

@author: al
"""

import sys
import numpy as np
import os.path
from mpi4py import MPI

import time

file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

from mpi_datatypes import mpi_dtype_int, mpi_size_int, np_dtype_int
from mpi_datatypes import mpi_dtype_float, mpi_size_float, np_dtype_float
from mpi_datatypes import mpi_dtype_bool, mpi_size_bool, np_dtype_bool


"""
Functions for creating MPI shared memory windows at the root process for all
the comm processes to access
"""
# Creation of shared memory window between processes of 'n' integers
def get_shared_win_int(comm, targetRank, rank, n):
    mem = MPI.Alloc_mem(n * mpi_size_int) if rank == targetRank else None
    return MPI.Win.Create(mem, disp_unit=mpi_size_int, comm=comm)
    
# Creation of shared memory window between processes of 'n' floats
def get_shared_win_float(comm, targetRank, rank, n):
    win_size = n * mpi_size_float if rank == targetRank else 0
    return MPI.Win.Allocate(size=win_size, disp_unit=mpi_size_float, comm=comm)

# Creation of shared memory window between processes of 'n' bools
def get_shared_win_bool(comm, targetRank, rank, n):
    mem = MPI.Alloc_mem(n * n * mpi_size_bool) if rank == targetRank else None
    return MPI.Win.Create(mem, disp_unit=mpi_size_bool, comm=comm)


"""
Functions called by the manager communicator to manage the status of worker
processes
"""

# Function for worker initialization
# Puts worker status at 1 on window or fails and exits
def mpi_init_workers_RMA(comm, rank, win):
    if ( rank == 0 ):
        buf = np.ones(1, dtype=np_dtype_int)
        win.Lock(rank=0)
        win.Put(buf, target_rank=0, target=(rank, 1, mpi_dtype_int))
        win.Unlock(rank=0)
        comm.Barrier()
        buf = np.frombuffer(win, dtype=np_dtype_int)
        if ( not np.all( buf == 1 ) ):
            # Send close signal to all workers
            nWorkers = comm.Get_size() - 1
            mpi_end_workers(nWorkers)
            print("[ERROR]: Workers can't be initialized", file=sys.stderr)
    else:
        buf = np.ones(1, dtype=np_dtype_int)
        win.Lock(rank=0)
        win.Put(buf, target_rank=0, target=(rank, 1, mpi_dtype_int))
        win.Unlock(rank=0)        
        comm.Barrier()
    return 1


def mpi_init_workers_P2P(comm):
    rnk, size = (comm.Get_rank(), comm.Get_size())
    # Set send buffer for all processes to 1
    sendGatherBuf = np.ones(1, dtype=np_dtype_int)
    # Set receive buffer only for manager process and initialize to 0
    if ( rnk == 0 ):
        recvGatherBuf = np.zeros(size, dtype=np_dtype_int)
    else:
        recvGatherBuf = None

    # Perform a non-blocking collective gather on manager process
    req = comm.Igather([sendGatherBuf, 1, mpi_dtype_int], [recvGatherBuf, 1, mpi_dtype_int], 0)
    
    # Time-out check for gather operation on all processes
    t0 = time.time()
    wait = True
    while wait:
        commOK = req.Test()
        if commOK:
            wait = False
        elif (time.time() - t0 > 10):
            print(f"[ERROR]: Process {rnk} timed out on init gather phase")
            wait = False
        time.sleep(0.1)
    
    # Proceed only if the gather request is True
    if ( commOK == False ):
        print(f"[ERROR]: Process {rnk} commOK is FALSE. EXITING")
        exit()
    
    # Verify that manager process has gathered from all processes
    if (rnk == 0):
        reqLSend = []
        # Send msg to al processes with comm check status 1 -> OK
        if np.all(recvGatherBuf == 1):
            outSendBuf = np.ones(1, dtype=np_dtype_int)
        else:
            print("[ERROR]: Manager failed to receive 1 from all workers." +
                  " EXITING")
            exit()
        for i in range(size):
            req = comm.Isend([outSendBuf, mpi_dtype_int], dest=i, tag=0)
            reqLSend.append(req)
    
    inSendBuf = np.zeros(1, dtype=np_dtype_int)
    reqRec = comm.Irecv([inSendBuf, mpi_dtype_int], source=0, tag = 0)
    
    # Time-out check for receiving green light from Manager to carry on
    t0 = time.time()
    wait = True
    SendOK = False
    RecvOK = False
    while wait:
        # Check for completion status of P2P communications
        if ( rnk == 0 and not SendOK):
            SendOK = MPI.Request.Testall(reqLSend)
        elif ( not RecvOK):
            RecvOK = reqRec.Test()
        elif ( rnk == 0 ):
            return recvGatherBuf
        else:
            return inSendBuf[0]
        # Check for time out
        if (time.time() - t0 > 10):
            wait = False
        time.sleep(0.1)
    
    print(f"[ERROR]: Process {rnk} timed out on init P2P comm phase")
    exit()


# Function called from manager process to end worker processes in the
# communicator 'comm'
# Sends a message with the list [-1, -1] (it doesn't matter the actual values)
# and tag 3, the real flag to be interpreted by workers as end signal
def mpi_end_workers(comm):
    size = comm.Get_size()
    # List of communication requests
    reqL = []
    # Send signal to end to all worker processes
    outBuf = np.array([-1, -1], np_dtype_int)
    for i in range(1, size):
        reqL.append(comm.Isend([outBuf, mpi_dtype_int], dest=i , tag=3))
    # Wait for communication to be completed
    MPI.Request.waitall(reqL)


# Send MPI signal to worker with process 'prc' in communicator 'comm' to find
# the porosity limit at cell 'nCell'
def mpi_worker_find_poroL(comm, prc, nCell):
    outBuf = np.array([nCell, 0], np_dtype_int)
    comm.Send([outBuf, mpi_dtype_int], dest=prc, tag=1)
    
    
# Send MPI signal to worker 'prc' to run the simulation 'nRun' at folder 'nFold'
def mpi_worker2folder(comm, prc, nRun, nFold):
    outBuf = np.array([nFold, nRun], np_dtype_int)
    comm.Send([outBuf, mpi_dtype_int], dest=prc, tag=2)
    

# Send MPI signal to prc to run the simulation nSim at cell nCell
# with the values a, v and [p]
def mpi_worker_simulate_cell_cond(comm, prc, nSim, nCell, cond):
    # Unpack simulation variables from cond
    a = cond[0]
    v = cond[1]
    p = cond[2]
    # Send info to process to enter simulation mode
    outBuf = np.array([nCell, nSim], np_dtype_int)
    comm.Send([outBuf, mpi_dtype_int], dest=prc, tag=2)
    # Send simulation variables to process
    outBuf = np.array([a, v, p[0], p[1], p[2], p[3]], np_dtype_float)
    comm.Send([outBuf, mpi_dtype_float], dest=prc, tag=21)

    

# Returns the porosity limits search status
def get_poro_status(win, winOwner, nV):
    # Get the information about porosity status in buffer
    pStatBuf = np.empty(nV, dtype=np_dtype_int)
    win.Lock(rank=winOwner, lock_type=MPI.LOCK_EXCLUSIVE)
    win.Get(pStatBuf, target_rank=winOwner, target=(0, nV, mpi_dtype_int))
    win.Unlock(rank=winOwner)
    return pStatBuf


# Returns the porosity limits array
def get_porosity_limits_array(win, winOwner, nV):
    pLimsBuf = np.empty(2*nV, dtype=np_dtype_float)
    win.Lock(rank=winOwner, lock_type=MPI.LOCK_EXCLUSIVE)
    win.Get(pLimsBuf, target_rank=winOwner)
    win.Unlock(rank=winOwner)
    return pLimsBuf

# Tries to get a lock for atomic operations on a lock flag RMA
def ask_for_lock(win, winOwner):
    # Set up buffers for getting and replacing
    prevStatus = np.empty(1, dtype=np_dtype_bool)
    newStatus = np.array([True], dtype=np_dtype_bool)
    win.Lock(rank=winOwner, lock_type=MPI.LOCK_EXCLUSIVE)
    win.Fetch_and_op(newStatus, prevStatus, winOwner, op=MPI.REPLACE)
    win.Unlock(rank=winOwner)
    return (not prevStatus[0])


# Releases lock in a shared control window
def release_lock(win, winOwner):
    newStatus = np.array([False], dtype=np_dtype_bool)
    win.Lock(rank=winOwner, lock_type=MPI.LOCK_EXCLUSIVE)
    win.Put(newStatus, target_rank=winOwner)
    win.Unlock(rank=winOwner)
    
# Gets Index for poro search from poro_Search array and updates the status
# for that index
def get_idx_poro_search_update(iStart, win, winOwner, rank, nV):
    # Get the up-to-date information about porosity status
    pStatBuf = np.empty(nV, dtype=np_dtype_int)
    # Get hold of the shared window for multiple accesses
    win.Lock(rank=winOwner, lock_type=MPI.LOCK_EXCLUSIVE)
    # Get the information about porosity status in buffer
    win.Get(pStatBuf, target_rank=winOwner, target=(0, nV, mpi_dtype_int))
#DEBUG
    print(f"Process {rank} lock on poroSearch window: {pStatBuf}")    
#END DEBUG
    # pre-set index to last possible + 1
    idx = nV
    # Look for an actual index
    for i in range(iStart, nV):
        if (pStatBuf[i] == -1):
            idx = i
            # Update idx in poroSearch array with the rank number (woking on it)
            newVal = np.array([rank], dtype=np_dtype_int)
            win.Put(newVal, target_rank=winOwner, target=(idx, 1, mpi_dtype_int))
            break    
    # Release lock on shared window
    win.Unlock(rank=winOwner)
#DEBUG
    print(f"Process {rank} got index {idx} to look for porosity limits")    
#END DEBUG
    return idx


# Update status of poro search and check whether this is the last update
def poro_search_update(win, winOwner, comm, idx, nV):
    rank = comm.Get_rank()
    size = comm.Get_size()
    # Get the up-to-date information about porosity status
    pStatBuf = np.empty(nV, dtype=np_dtype_int)
    # Get hold of the shared window for multiple accesses
    win.Lock(rank=winOwner, lock_type=MPI.LOCK_EXCLUSIVE)
    # Get the information about porosity status in buffer
    win.Get(pStatBuf, target_rank=winOwner, target=(0, nV, mpi_dtype_int))
#DEBUG
    print(f"Process {rank} lock on poroSearch window: {pStatBuf}")    
#END DEBUG
    # Update idx in poroSearch array with the size number -> poro Limits found
    pStatBuf[idx] = size    
    newVal = np.array([size], dtype=np_dtype_int)
    win.Put(newVal, target_rank=winOwner, target=(idx, 1, mpi_dtype_int))
    win.Unlock(rank=winOwner)
    return (pStatBuf == size).all()
    

# Update porosity limits list in shared poroLimits
def update_poro_values(win, winOwner, idx, pmin, pmax):
    # create temporal buffer with values to update
    buf = np.array([pmin, pmax], dtype=np_dtype_float)
    # Put the values in the idx place of poroLimits
    win.Lock(rank=winOwner, lock_type=MPI.LOCK_SHARED)
    win.Put(buf, target_rank=winOwner, target=(2*idx, 2, mpi_dtype_float))
    win.Unlock(rank=winOwner)
    

# Update status of process status shared window
def update_process_status(win, winOwner, rank, status):
    buf = np.array([status], dtype=np_dtype_int)
    win.Lock(rank=winOwner, lock_type=MPI.LOCK_SHARED)
    win.Put(buf, target_rank=winOwner, target=(rank, 1, mpi_dtype_int))
    win.Unlock(rank=winOwner)
    
    
def lock_release_target(win, winOwner, rank):
    win.Lock(rank=winOwner, lock_type=MPI.LOCK_SHARED)
    win.Unlock(rank=winOwner)