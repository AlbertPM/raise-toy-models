#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  3 17:02:36 2023

@author: al
"""

import math
import numpy as np
from scipy.stats import norm
from scipy.optimize import curve_fit
from scipy.special import ndtri

import matplotlib
#matplotlib.use("Agg")
#matplotlib.use("nbagg")
import matplotlib.pyplot as plt

from time import sleep


def derivatives(xarr, yarr):
    N = len(xarr)
    darr = []
    ddarr = []
    # Loop that looks for derivatives at each available point i
    for i in range(N):
        b = []
        M = []
        # Loop that files the rows of the system to solve 
        for j in range(N):
            if j != i:
                b.append(yarr[j] - yarr[i])
                # We look only for derivatives using cuadratic aproximations
                # at most
                Mrow = [(xarr[j] - xarr[i])**(k+1) / math.factorial(k+1) 
                        for k in range(min(N-1, 2))]
                M.append(Mrow)
        b = np.array(b)
        M = np.array(M)
        # Cuadratic function is approximated for an arbitrary number of
        # point pairs using least squares
        X = np.linalg.lstsq(M, b, rcond=None)[0]
        darr.append(X[0])
        ddarr.append(X[1])
    return [darr, ddarr]


def fakeDVFunction(p):
    M = 0.1     # Increase to grow function
    a = 1.5       # Increase to widen function
    b = 1       # Increase to displace function to the right
    return M/2 * (1 + np.tanh((np.log10(p) - b)/a ))


# Find the maximum porosity to simulate at each folder conditions (v and ang)
# tempF     -> Pathname of the temporal folder where simulations will run
# fold      -> Name of the folder with data for the simulation
# case      -> Base name of the files with data for the simulation
# nF        -> Index of the folder where a parallel simulation is run
def run_find_max_poro_A():
       
    def propose_new_limits(xarr, yarr, xsol0):
        # Try to fit points with a normal cdf
        fNorm = lambda x_f,mu,sigma, M: M*norm(mu,sigma).cdf(x_f)
        mu, sigma, M = curve_fit(fNorm, xarr, yarr)[0]       
        # Get the points where CDF is 0.01 and 0.99
        xl = ndtri(0.001) * sigma + mu
        xr = ndtri(0.999) * sigma + mu
        
        return np.array([xl, xr])

    def adjust_new_points(pLN, lP):
        N = len(lp)
        pNew = []
        for pL in pLN:
            idx = np.searchsorted(lP, pL)
            if idx == 0:
                pNew.append(1.5*lP[0]-0.5*lP[1])
            elif idx == N:
                pNew.append(1.5*lP[N-1]-0.5*lP[N-2])
            else:
                pNew.append(0.5*(lP[idx]+lP[idx-1]))
        
        return np.array(pNew)
    
    # Initially defined porosity limits based on theoretical order of magnitude
    ptheo = [1e-4, 1e4]    

    # Pass to log and expand for derivatives search
    lp = np.log10(ptheo)
    varlp = lp[1]-lp[0]
    lp = np.array([lp[0]-varlp/6, lp[0], lp[0]+varlp/3,
                   lp[1]-varlp/3, lp[1], lp[1]+varlp/6])
    
    # Index of starting proposed porosity limits
    pIdx = [1, 4]
    
    # Simulate and obtain normalized velocity field average variation (NVFAV)
    DV = []         # List of NVFAV values
    # Populate DV list
    for p in list(10**lp):
        # Add NVFAV value against homogeneous field with v and a
        DV.append(fakeDVFunction(p))
    DV = np.array(DV)
    
    pLim = np.array([lp[pIdx[0]], lp[pIdx[1]]])
    # Flag for porosity found
    notPoroLimitsFound = True                          
    while notPoroLimitsFound:
        # Propose new points as the porosity limits
        pLimOld = pLim
        pLim = propose_new_limits(lp, DV, pLimOld)
        # Check convergence
        pRange0 = pLimOld[1] - pLimOld[0]
        pRange = pLim[1] - pLim[0]
        print(len(lp))
        print(pRange0, pRange)
        plot(pLim[0], pLim[1], lp, DV)
        sleep(1)
        if abs(pRange - pRange0) < 1e-2 * max(pRange, pRange0) :
            notPoroLimitsFound = False
            next
        # find 2 new points to simulate
        pNew = adjust_new_points(pLim, lp)
        # Simulate for new points NVFAV
        newDV = []
        for p in list(10**pNew):
            # Add NVFAV value against homogeneous field with v and a
            newDV.append(fakeDVFunction(p))
        newDV = np.array(newDV)
        # Insert new points in existing arrays
        insertIdx = np.searchsorted(lp, pNew)
        lp = np.insert(lp, insertIdx, pNew)
        DV = np.insert(DV, insertIdx, newDV)
        
    return float(pLim[0]), float(pLim[1]), lp, DV



# create a graph with predicted porosity vs. real porosity
def plot(pmin, pmax, lp, DV):
    parr = np.linspace(-6, 6, 1000)
    fig, ax = plt.subplots(figsize=(8,8))
    
    ax.plot(parr, [fakeDVFunction(p) for p in 10**parr])
    
    ax.scatter(lp, DV)
    ax.scatter(pmin, 0)
    ax.scatter(pmax, 0)

    plt.show()


print(run_find_max_poro_A())

