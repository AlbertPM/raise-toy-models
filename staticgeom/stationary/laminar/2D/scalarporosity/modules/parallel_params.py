#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 18:44:58 2023

@author: al

Parameters that define the number of simulations to run and
the values to use on them

multPorMax          --- Order of magnitude for initial porosity limits
obstLen             --- Characteristic length of hr obstacle
visc                --- Fluid viscosity

poroType            --- Type of porosity used in the surrogate
poroDistrib         --- Statistical shape of porosity distribution

aRange              --- Range of incoming velocity degrees to simulate
vRange              --- Range of incoming velocity to simulate
nAbins              --- Number of angle subdivisions for simulation
nVbins              --- Number of velocity subdivisions for simulation
nAVcond             --- Number of angle-velocity conditions to parallelize

vDistType           --- Statistical shape of velocity distribution
vBinsLims           --- Array with velocity limits for velocity subdivisions


"""

import sys
import numpy as np



# Define whether simulation is stationary or transitory
# Not currently used
timeDependent = False
timeSteps = 1

# Limiting order of magnitude multiplier for porosity value contribution
# and physical fluid variables related to max porosity value calculation
multPorMax = 1e3
obstLen = 0.024     # characteristic length
visc = 1e-3         # fluid viscosity

# Porosity type
# 'scalar' / 'rotBasis' / 'changeBasis' / 'constantTxx'
poroType = 'scalar'
TxxFraction = 0.5

# Porosity distribution
# 'uniform' / 'triangular' / 'log'
poroDistrib = 'log'

# Define the ranges for variables to simulate: Angles and velocities
aRange      = [0, 20]           # degrees
vRange      = [0.01, 1.0]         # velocity

# Define the number of simulations that will be run.
# Velocity and angle variables are split in nV and nA bins (Preliminary
# solutions can be used on the resulting nA*nV spaces)
nAbins = 10
nVbins = 10

nAVCond = nVbins * nAbins

aBinsLims = [aRange[0]+(aRange[1]-aRange[0])*i/nAbins for i in range(nAbins+1)]


# Type of velocity distribution
# 'geometric' / 'uniform' / 'log'
vDistType = 'uniform'

# Geometric distribution
if (vDistType == 'geometric'):
    mY = 5      # Height factor between lowest and highest velocity bins 
    r = mY**(1/(nVbins-1))  # ratio of variation between consecutive bins length
    S = 0       # Ratio between total length and first bin length
    for i in range(nVbins):
        S = S + r**i
    h1 = (vRange[1] - vRange[0]) / S
    vBinsLims = [vRange[0]]
    for i in range(nVbins):
        last = vBinsLims[-1]
        vBinsLims.append(last + h1*r**i)
# Uniform distribution
elif (vDistType == 'uniform'):
    vBinsLims = [vRange[0]+(vRange[1]-vRange[0])*i/nVbins for i in range(nVbins+1)]
# Logaritmic distribution
elif (vDistType == 'log'):
    vLogRange = np.log(vRange)
    vBinsLims = [np.exp(vLogRange[0]+(vLogRange[1]-vLogRange[0])*i/nVbins) 
                 for i in range(nVbins+1)]
# Exit in case of wrong velocity distribution definition
else:
    print("[ERROR]: Not recognized velocity distribution", file=sys.stderr)
    exit()
