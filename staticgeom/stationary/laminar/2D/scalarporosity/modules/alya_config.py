#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 15:19:18 2023

@author: al

Module with necessary functions to overwrite Alya configuration files

"""

import sys

# Change the Cosinus 'value' of the angle where the boundary condition changes
# from impossed velocity to free condition (needed to avoid ouptut flow artifacts)
# path to case.nsi.dat file is required
def set_boundary_cos_limit(nsiFile, cosValue):
    
    # Store file lines as list of strings
    with open(nsiFile, "r") as f:
        lines = f.readlines()
    
    # Search for the line defining cosinus value and modify it with 'cosValue'
    for i,line in enumerate(lines):
        if ( -1 != line.find("INFLOW_COSINE") ):
            lines[i] = f"    INFLOW_COSINE={str(round(cosValue, 6))}\n"
    
    # Rewrite the whole file including the modified line
    with open(nsiFile, "w") as f:
        f.writelines(lines)



# Sets the angle to rotate the mesh in the .ker.dat file
# the effect is like rotating the velocity vector the same amount in the
# simulation configuration files
#
# DEPRECATED on later vesions of the scripts.
def set_mesh_rotation(kerFile, angle):
    
    # Store file lines as list of strings
    with open(kerFile, "r") as f:
        lines = f.readlines()

    # Search for the line defining angle value and modify it with 'value'
    for i,line in enumerate(lines):
        if ( -1 != line.find("ROTATION:") ):
            lines[i] = f"    ROTATION: Z, ANGLE=-{angle}\n"
    
    # Rewrite the whole file including the modified line
    with open(kerFile, "w") as f:
        f.writelines(lines)
        


# sets the modulus of the velocity in the .ker.dat file
#
# DEPRECATED on later versions of the scripts where velocity is set as mod
# and angle in a fixed mesh
def set_V_mod(kerFile, mod):
    
    # Store file lines as list of strings
    with open(kerFile, "r") as f:
        lines = f.readlines()

    # look for the lines where x and y components of velocity are specified
    # and update them with mod
    for i,line in enumerate(lines):
        if ( -1 != line.find("FUNCTION=INFLOW, DIMENSION=2") ):
            lines[i+1] = f"      {mod}*cos(0/180*pi(1))\n"
            lines[i+2] = f"      {mod}*sin(0/180*pi(1))\n"

    # Rewrite the file with the modified lines
    with open(kerFile, "w") as f:
        f.writelines(lines)


# sets the modulus and angle of the velocity in the .ker.dat file symultaneously
def set_V(kerFile, mod, ang):
    
    # Read the file lines and store them in a list
    with open(kerFile, "r") as f:
        lines = f.readlines()

    # Search for the line with the definition of the inflow function
    for i,line in enumerate(lines):
        if ( -1 != line.find("FUNCTION=INFLOW, DIMENSION=2") ):
            lines[i+1] = f"      {mod}*cos({ang}/180*pi(1))\n"
            lines[i+2] = f"      {mod}*sin({ang}/180*pi(1))\n"

    # Rewrite the file with the modified lines
    with open(kerFile, "w") as f:
        f.writelines(lines)


# sets the porosity of the surrogate material mesh in the .ker.dat file
# The function expects poro as int or float for scalar porosity and a list for
# tensor porosity (with not defined maximum number of components)
def set_porosity(kerFile, poro):
    
    # Discriminate scalar and tensor porosities and set line to write 
    if (type(poro) == int or type(poro) == float):      # Scalar
        pstr = str(poro)
        ptype = "      POROSITY"
    elif (type(poro) == list):                          # Tensor
        pstr = str(poro[0])
        ptype = "      ANIPOROSITY"
        for p in poro[1:]:
            pstr = f"{pstr},{p}"
    else:
        print(type(poro))
        exit("[ERROR]: alya_config.py/set_porosity -> bad porosity definition")
    poroLine = f"{ptype}:  CONSTANT: VALUE={pstr}\n"

    # Read case.ker file lines as list of strings
    with open(kerFile, "r") as f:
        lines = f.readlines()

    # Search for the line defining porosity and modify it with the 'poro' value
    found = False
    for i,line in enumerate(lines):
        if ( -1 != line.find("$POROSITY_TAG") ):
            lines[i+1] = poroLine
            found = True
    if not found: print("[WARNING]: No POROSITY_TAG anchor found", file=sys.stderr)

    # Rewrite the whole file including the modified line
    with open(kerFile, "w") as f:
        f.writelines(lines)
        
        
# sets the simulation variable values for the 'case' inside 'folder':
# velocity angle, modulus and porosity
def set_sim_vars(fold, case, ang, vel, poro=0):
    angS = str(round(ang, 6))               # angle in degrees as a string
    velS = str(round(vel, 6))               # velocity as a string
    fileName = f"{fold}/{case}.ker.dat"   # file to be modified
    set_V(fileName, velS, angS)             # update velocity in the .ker.dat file
    set_porosity(fileName, poro)            # update porosity in the .ker.dat file
    
    