#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 10 18:28:05 2022

@author: al
"""
import mpi4py
mpi4py.rc.thread_level = 'multiple'

from mpi4py import MPI
import numpy as np


import sys
from os import listdir
import time


from modules.mpi_functions import mpi_end_workers, mpi_worker_find_poroL, \
                                mpi_worker2folder, mpi_init_workers_P2P, \
                                get_poro_status, \
                                get_idx_poro_search_update, \
                                update_process_status, update_poro_values, \
                                poro_search_update, mpi_worker_simulate_cell_cond

from modules.mpi_datatypes import mpi_dtype_int, np_dtype_int
from modules.mpi_datatypes import mpi_dtype_float, np_dtype_float

import modules.parallel_params as pp

from modules.parallel_common_functions import   get_limits_for_cell, \
                                                generate_poro_values
    
from modules.parallel_manager_functions import  find_idle_process, \
                                                find_folder_to_poro_P2P, \
                                                count_simulated_cases, \
                                                rm_temp_folders, \
                                                exists_saved_porosities, \
                                                read_porosity_limits, \
                                                save_porosity_limits, \
                                                check_completion_folder_num_error
                                                
from modules.parallel_worker_functions import get_poro_lim_values, \
                                            run_conti_case_P2P, \
                                            run_find_max_poro, \
                                            check_pending_poro_limits
    


#import pyextrae.mpi as pyextrae

#%%


"""
function definitions
"""

#%%


if __name__ == "__main__":

    import argparse

    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--folder", type=str, required=True,
    	help="Name of the folder with the model")
    ap.add_argument("-c", "--case", type=str, required=True,
    	help="Name of the case to simulate")
    ap.add_argument("-N", "--Num", type=int, required=True,
        help="Max number of total simulations to run")
    ap.add_argument("-hr", "--highRes", action='store_true',
        help="Mark whether current simulations are for high resolution model")
    ap.add_argument("-tf", "--tempfolder", type=str, required=False,
        help="Location of temporal disk storage (defaults to . if unset)")
    args = vars(ap.parse_args())


    # name of folder with the case to simulate in Alya
    caseFolder = args["folder"]
    # name of the case to simulate in Alya
    caseName = args["case"]
    # temporal folder
    if args["tempfolder"] == None:
        tempFolder = "."
    else:
        tempFolder = args["tempfolder"]

    
    # MPI communication between python processes dims and rank
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    
    # number of processes
    nProcs = size
    # number of cells in the angle/velocity grid configuration space
    nCells = pp.nAVCond
    # number of simulations per cell
    nSim_Cell = args["Num"] // nCells
    # real number of total simulations to make
    totSim = nSim_Cell * nCells

    # Print MPI processes and folders and check Number of simulations to be done
    if rank == 0:
        print("---------------------------------")
        print(f"Number of workers: {nProcs}")
        print(f"Number of Cells: {nCells}")
        print(f"Actual total number of simulations: {totSim}")
        print(f"Number of sims per folder: {nSim_Cell}")
        print(f"MPI version: {MPI.Get_version()}")
        print("---------------------------------")

    # Check that there is at least 1 simulation per cell
    if ( nSim_Cell < 1 ):
        if rank == 0:
            print("[ERROR]: No simulations to be run, increase number", file=sys.stderr)
        exit()     
       

    # Creation of control lists in manager process to track:
    # Status of workers                 -> prcStatus[nWorkers]
    # cases simulated at each folder    -> foldList[nFolders]
    # folder completion status          -> notCompleted[nFolders]
    # folder locking status             -> foldLocked[nFolders]
    # process assigned to folders       -> procOnFolder[nFolders]
    # porosity limit found status       -> poroLimitFound[nFolders]
    # porosity values                   -> poroValues[nFolders]
    if ( rank == 0 ):
          
        # List with process status
        # for process status tracking -> window of 'nWorkers' integers
        # 0 -> Worker not contacted
        # 1 -> Idle
        # 2 -> Looking for porosity limits
        # 3 -> Simulating
        # 4 -> Ended
        # Initialize the array to 0 (not contacted)
        prcStatus = np.zeros(nProcs, dtype=np_dtype_int)
        
        # Set random seed
        seed = int.from_bytes(f"{42}".encode(), 'big')
        rng = np.random.default_rng(seed)
        
        # LIst with random conditions to simulate for each case
        # [A, V, [P]]
        # At this stage initialize [P] to 0
        simValuesList = [None] * totSim
        for iCell in range(nCells):
            aBin, vBin = get_limits_for_cell(pp.aBinsLims, pp.vBinsLims, iCell)
            # get random uniform distributed values of angles and velocity inside
            # local cell limits (global velocity is not uniform)
            ang = rng.uniform(aBin[0], aBin[1], nSim_Cell)
            vel = rng.uniform(vBin[0], vBin[1], nSim_Cell)
            P = [0, 0, 0, 0]
            for i in range(nSim_Cell):
                simValuesList[iCell * nSim_Cell + i] = [ang[i], vel[i], P[:]]
        
        # Fill porosity related arrays
        # for Porosity search status
        # 0 -> Not started
        # 0 < n < nProcs -> Being searched by proc n
        # nProcs -> Porosity search completed
        poroSearch = np.zeros(pp.nVbins, dtype=np_dtype_int)
        
        poroLimits = np.zeros(2*pp.nVbins, dtype=np_dtype_float)
        # Read porosity limits if saved file exists
        pLimitsSaved = exists_saved_porosities("porosities.bin")
        if not args["highRes"] and pLimitsSaved:
            poroLimits[:] = read_porosity_limits(pp.nVbins, "porosities.bin")
            poroSearch[:] = nProcs
            print(f"Porosities loaded: {poroLimits}")
            # Fill simValuesList with the porosities found for pSrchIdx V
            for vIdx in range(pp.nVbins):
                generate_poro_values(simValuesList, nSim_Cell, vIdx, poroLimits)
        elif not args["highRes"]:
            print("No porosity limits file to load")
        else:
            pLimitsSaved = True
            poroSearch[:] = nProcs
            print("High resolution simulations. No porosities needed.")
           
        # List with numbers of cases completed per each folder
        simCellList = np.zeros(nCells, int)       
        # Count completed cases per cell from 'results' folder
        for i in range(nCells):
            simCellList[i] = count_simulated_cases("./results", f"{caseFolder}-{i}")
        print(f"simCellList: {simCellList}")
        # List with index of process assigned to each simulation
        procDidSimulation = np.array([-1]*nProcs, dtype=np_dtype_int)
        
    else:
        prcStatus = 0



    comm.Barrier()
    
    # Process initialization. Should return 1 if everything is ok
    prcStatus = mpi_init_workers_P2P(comm)

    # Set approximate global initial time (not MPI synchronized)
    t0 = time.time()
    
    # Start Manager process main loop
    if (rank == 0):
        phase = 1
        # Check partial completion status returns index of cell with
        # wrong number of simulated cases or -1 if any
        cellError = check_completion_folder_num_error(simCellList, nSim_Cell)
        if ( cellError > -1 ):
            t = time.time()
            msg = f"[ERROR]: Cases for {caseFolder}-{i} > number of runs"
            print(f"<{int(t-t0)} s><{rank}>: {msg}")
            phase = 3
        # Initialize an array of request arrays and an status variable. (MPI)
        reqL = [MPI.REQUEST_NULL] * nProcs
        status = MPI.Status()
    while ( rank == 0):
        
        # Select an Idle Worker
        proc = find_idle_process(prcStatus)
        
        # Set time
        t = time.time()

        # PHASE 1: Assign processes (not manager) to look for porosity limits
        # while there are slots open for searching
        while ( phase == 1 and proc != 0 ):
            msg = f"Poro limits search status: {poroSearch}"
            print(f"<{int(t-t0)} s><{rank}>: {msg}")                   
            # Search for poro Limits still not found
            idxPSrch = find_folder_to_poro_P2P(poroSearch)
            # idx is -1 in case all the porosity limits have been found
            if (idxPSrch == -1):
                msg = "No more poro limits to search. PHASE -> 2"
                print(f"<{int(t-t0)} s><{rank}>: {msg}")
                phase = 2
            else:
                msg = f"Poro limits can be searched in index {idxPSrch}"
                print(f"<{int(t-t0)} s><{rank}>: {msg}")
                # Assign the idle process to look for porosity
                mpi_worker_find_poroL(comm, proc, idxPSrch)
                poroSearch[idxPSrch] = proc
                prcStatus[proc] = 2
                # Create request for receiving porosity Limits when found
                reqL[proc] = comm.Irecv([poroLimits[2*idxPSrch:2*idxPSrch+2], 
                                         mpi_dtype_float], source = proc, tag = 1)
            # Select an Idle Worker
            proc = find_idle_process(prcStatus)
        
        
        while ( phase == 2 and proc != 0 ):
            # Save porosity limits to binary field when all of them have been
            # found
            if not(pLimitsSaved) and np.all(poroSearch == nProcs):
                save_porosity_limits(pp.nVbins, poroLimits, "porosities.bin")
                pLimitsSaved = True               
            
            # Out of loop if no poro limits have been found yet
            if np.all(poroSearch != nProcs):
                break
            
            # Change to Phase 3 if all simulations have been sent
            # and get out of loop
            if np.all(simCellList == nSim_Cell):
                msg = "No more simulations to send. PHASE -> 3"
                print(f"<{int(t-t0)} s><{rank}>: {msg}")
                phase = 3
                break
            
            # Get a cell where simulations can be run (with minimum simulations)
            iCell = [ i for i, n in enumerate(simCellList) 
                      if poroSearch[i % pp.nVbins] == nProcs 
                      and simCellList[i] < nSim_Cell ]
            nCell = [ n for i, n in enumerate(simCellList) 
                      if poroSearch[i % pp.nVbins] == nProcs 
                      and simCellList[i] < nSim_Cell ]
            
            # Check that there are available cells
            if ( len(nCell) == 0 ):
                break
            
            cellToSim = iCell[nCell.index(min(nCell))]
            nSim = simCellList[cellToSim] + 1
            
            # Get conditions to simulate in the cell
            avpCond = simValuesList[nSim_Cell * cellToSim + 
                                    simCellList[cellToSim]]
            
            # Send process to simuate with the given variables and update Status
            mpi_worker_simulate_cell_cond(comm, proc, nSim, cellToSim, avpCond)
            prcStatus[proc] = 3
            
            # Update value in simCellList
            simCellList[cellToSim] = nSim
            
            # Create a request to be notified when process ends
            reqL[proc] = comm.Irecv([procDidSimulation[proc:proc+1], mpi_dtype_int], 
                                    source = proc, tag = 2)
            
            # Select an Idle Worker
            proc = find_idle_process(prcStatus)
        
            
        # Phase 3 is closing and cleaning up
        if ( phase == 3 and np.all(prcStatus == 1)):
            # clean temporary folders
            rm_temp_folders(tempFolder, caseFolder, nProcs)
            # end workers
            mpi_end_workers(comm)
            print("---------------------------------")
            print("Ending Manager Process")
            print(f"processes status: {prcStatus}")
            print(f"List of simulations done: {simCellList}")
            print("---------------------------------")
            break
        

        # Test for completed requests or wait for one
        reqIdx, reqCompleted = MPI.Request.Testany(reqL)
        if ( not reqCompleted ):
            msg = "Waiting on request completion"
            print(f"<{int(t-t0)} s><{rank}>: {msg}")
            reqIdx = MPI.Request.Waitany(reqL)
            
        # If process was looking for porosities, they have been received
        if ( prcStatus[reqIdx] == 2 ):
            # Set process to idle
            prcStatus[reqIdx] = 1
            # Set value in poroSearch to nProcs (== poro limits found)
            pSrchIdx = [i for i, proc in enumerate(poroSearch) if 
                             proc == reqIdx][0]
            poroSearch[pSrchIdx] = nProcs
            
            # Fill simValuesList with the porosities found for pSrchIdx V
            generate_poro_values(simValuesList, nSim_Cell, pSrchIdx, poroLimits)

        # If process was simulating, acknowledge end
        if (prcStatus[reqIdx] == 3 ):
            # Set process to idle
            prcStatus[reqIdx] = 1

        
        print("---------------------------------")
        print("End of manager loop")
        print(f"processes status: {prcStatus}")
        print(f"List of simulations done: {simCellList}")
        print("---------------------------------")
        
                
    # Worker process main loop    
    while ( rank != 0 ):        
        # status for communication
        status = MPI.Status()
        # tag=1 to order worker to find poro limit
        # tag=2 to order worker to start simulation
        # tag=3 to order the worker to terminate
        inBuf = np.zeros(2, dtype=np_dtype_int)
        comm.Recv([inBuf, mpi_dtype_int], source=0, tag=MPI.ANY_TAG, status=status)
        
        # look for porosity limit
        if status.tag == 1:
            poroIdx = inBuf[0]
            # look for 'poroLimit'
            t = time.time()
            msg = f"Searching porosities on: {poroIdx}"
            print(f"<{int(t-t0)} s><{rank}>: {msg}")
            # look for 'poroLimit'
            pmin, pmax = run_find_max_poro(tempFolder, caseFolder, 
                                        caseName, rank, poroIdx)
            # Send porosity value to manager process
            t = time.time()
            msg = f"Sending poro limits {pmin}, {pmax} to manager"
            print(f"<{int(t-t0)} s><{rank}>: {msg}")
            outBuf = np.array([pmin, pmax], np_dtype_float)
            comm.Send([outBuf, mpi_dtype_float], dest=0, tag=1)  
        
        if status.tag == 2:
            nCell = inBuf[0]
            nSim = inBuf[1]
            # Acknowledge simulation command
            t = time.time()
            # Receive simulation data
            inBuf = np.zeros(6, dtype=np_dtype_float)
            comm.Recv([inBuf, mpi_dtype_float], source=0, tag=21, status=status)
            # Run simulation
            simData = [inBuf[0], inBuf[1], [inBuf[2], inBuf[3], inBuf[4], inBuf[5]]]
            msg = f"Simulation {nSim} on cell {nCell} -> {simData}"
            print(f"<{int(t-t0)} s><{rank}>: {msg}")
            run_conti_case_P2P(tempFolder, caseFolder, caseName, 
                               nCell, rank, simData, args["highRes"])
            msg = f"Simulation {nSim} on cell {nCell} completed"
            print(f"<{int(t-t0)} s><{rank}>: {msg}")
            outBuf = np.array([nSim], np_dtype_int)
            comm.Send([outBuf, mpi_dtype_int], dest=0, tag=2)  
            
        
        elif status.tag == 3:
            msg = f"Process {rank} TERMINATING"
            t = time.time()
            print(f"<< {int(t-t0)} s >>: {msg}")
            break


    exit()
