#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 12:51:41 2023

@author: al
"""

import os
import sys
import torch
import numpy as np
import sympy as sp

current_dir = os.path.dirname(os.path.realpath(__file__))

from common.alya_dataset import AlyaDataset
from model_surrogate.convolutional_network import ConvolutionalNetwork
from model_hires.CNN_highResolution import ConvolutionalNetwork_HR

import argparse

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", type=str, required=True,
                help="Name of HR model")
ap.add_argument("-ch", "--cnnHR", type=str, required=True,
                help="Name of trained HR CNN")
ap.add_argument("-cs", "--cnnS", type=str, required=True,
                help="Name of trained surrogate CNN")
args = vars(ap.parse_args())

model   = args["model"]
cnnS    = args["cnnS"]
cnnHR   = args["cnnHR"]

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

dataset = AlyaDataset(f"{current_dir}/predict/{model}", True, f"{cnnS}")

model = torch.load(f"ai-model/model_hires/{cnnHR}", map_location=device).to(device)
model.eval()

pmin = model.pmin
pmax = model.pmax

v = dataset.vIn
a = dataset.ang
p = dataset.y_data

print(p)

for sPor in p:
    for i, x in enumerate(sPor):
        sPor[i] = x * (pmax[i] - pmin[i]) + pmin[i]

print(p)

tVU = dataset.x1_data
with torch.no_grad():
    tensVU = tVU.to(device)    
    por = model.real_output(tensVU)


def transform2Diag(po):
    if type(po) == list:
        porM = sp.Matrix( [ [po[0], po[2]], [po[1], po[3]] ] )
        S, D = porM.diagonalize()
        eigVals = [ [D[0], D[1]], [D[2], D[3]] ]
        eigVecs = [ [S[0], S[2]], [S[1], S[3]] ]
        angles = [ np.arctan( float(S[2]/S[0]) ) * 180/np.pi,
                   np.arctan( float(S[3]/S[1]) ) * 180/np.pi]
        print(f"eigenvalues: {eigVals}")
        print(f"eigenvectors: {eigVecs}")
        print(f"angles: {angles}")


for i in range(len(v)):
    print("-------------------------")
    print(f"angle: {a[i]} | velocity: {v[i]}")
    print(f"Porosity SURROGATE: {p[i].tolist()}")
    transform2Diag(p[i].tolist())
    print(f"Porosity HIGH RESOLUTION: {por[i].tolist()}")
    transform2Diag(por[i].tolist())
    print("-------------------------")
