// Gmsh project created on Wed Dec 22 20:54:51 2021
SetFactory("OpenCASCADE");
//
Rinf = DefineNumber[ 0.5, Name "Parameters/Rinf" ];
Rs = DefineNumber[ 0.02, Name "Parameters/Rs" ];
//
meshmult = 0.5;		// mesh refinement multiplication parameter (bigger = finer)

// Define 2 circles, one on the infinity boundary and other surrounding the surrogate area
// This is done to be able to define different mesh sizes on those lines

Circle(1) = {0, 0, 0, Rinf};
Circle(2) = {0, 0, 0, Rs};

// Create Curve loops from circles to be able to use them to get plane surfaces

Curve Loop (1) = {1};
Curve Loop (2) = {2};

//-------------------------------------------------------------------------
// Pattern creation or square surrogate substitution
//
lpatt = 0.01;            // ortogonal distance between elemnts in the matrix
rpatt = 0.002;           // radius of obstacle cylinders
//--- Variation for the pattern of obstacles start here
RectPattListLines[] ={};      // List of rectangular fins to export as boundaries
RectLooPattList[] = {}; // List of rectangular Loops for obstacles
Lsq = 0.012;		// half distance of obstacle area
hrect = 0.001;		// Height of heatsink fins
N = 8;			// Number of fins
Fdist = (2 * Lsq - hrect) / (N - 1);	// separation between fin centers
For i In {1:N/2}
    j1 = 2 + 2*i -1;
    j2 = 2 + 2*i;
    Vx = 0.012;
    Vy = -Fdist/2 + Fdist * i - hrect/2;
    Rectangle(j1) = {-Vx,  Vy, 0, 2*Vx,  hrect};
    Rectangle(j2) = {-Vx, -Vy, 0, 2*Vx, -hrect};
    lidx1 = 2 + (i-1)*8 + 1;
    lidx2 = 2 + i*8; 
    RectPattListLines[] += {lidx1 : lidx2};
    RectLooPattList[] += {j1, j2};
EndFor
//--- End of Variation
//-------------------------------------------------------------------------
// Create the plane surfaces (Same lines can be used for both cases

Plane Surface(1) = {1, 2};
Plane Surface(2) = {2, RectLooPattList[]};
Delete {Surface{3 : N+2};}

// Using mesh size applied to the points of the circles
MeshSize{ PointsOf{ Curve{1}; } }                   	= 0.02  / meshmult;
MeshSize{ PointsOf{ Curve{2}; } }                   	= 0.001 / meshmult;
// Use this line in case of using the matrix pattern...
MeshSize{ PointsOf{ Curve{RectPattListLines[]}; } }     = 0.0001/ meshmult;

// Physical entities definition
Physical Line(     "Infty_B")   	= {1};
Physical Line(     "Obst_B")    	= {RectPattListLines[]};
Physical Surface(   "OutDomain")        = {1};
Physical Surface(   "InDomain")         = {2};

