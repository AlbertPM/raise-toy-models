#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 19:01:56 2023

@author: al
"""


import argparse

import os
from os import listdir
from os.path import exists,isfile, join

import random


# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-nRand", "--numRandomization", type=int, required=True,
	help="Integer number to set the randomizer seed")
ap.add_argument("-rdf", "--rawDataFolder", type=str, required=True,
	help="Name of the case to simulate")
ap.add_argument("-f", "--folder", type=str, required=True,
	help="Name of the simulated case to split")
ap.add_argument("-c", "--case", type=str, required=True,
	help="Name of the directory where case data are split")
args = vars(ap.parse_args())


# name of folder with the case to simulate in Alya
caseFolder = args["folder"]
# name of the case to simulate in Alya
caseName = args["case"]

testSplit = 0.1

# folder with the raw Data
rFName = args["rawDataFolder"]
rawDataFolder = r"./" + f"{rFName}" +r"/"

# Get a list with all files in 'folder'
fileList = [f for f in listdir(rawDataFolder) if isfile(join(rawDataFolder, f))]

# Get a list with the number of folder used during parallelization of simulations
simFolderList = []
# Get a list with the number of cases simulated at each folder number
numData = []
for f in fileList:
    nameSplit = f.split('-')        # Split the name of the file
    # Only look for the caseFolder results
    if (nameSplit[0] == caseFolder):
        # In case the folder number is not yet initialized...
        if ( simFolderList.count(int(nameSplit[1])) == 0 ):
            # ...Append folder number to list of simulated folders...
            simFolderList.append(int(nameSplit[1]))
            # and append a new counter for that folder
            numData.append(1)
        # In case it is...
        else:
            # ...Look for the index in the list of that folder number...
            idx = simFolderList.index(int(nameSplit[1]))
            # and increase the counter at that index
            numData[idx] += 1

# Pack both lists and sort by folder number
packList = list(zip(simFolderList, numData))
packList.sort(key=lambda x: x[0])

print(packList)

# Check for existence and creation of test/train folders
trFolder = f"./{caseName}_train/"
if (exists(trFolder)):
    exit("Train folder already exists")
else:
    os.mkdir(trFolder)
tsFolder = f"./{caseName}_test/"
if (exists(tsFolder)):
    exit("Test folder already exists")
else:
    os.mkdir(tsFolder)

# Random split of files 
random.seed(args["numRandomization"])
for d in packList:
    subset = random.sample(range(d[1]), int(testSplit*d[1]+0.5))
    subFileList = [f for f in fileList if int(f.split('-')[1]) == d[0] ]
    for i in range(d[1]):
        if i in subset:
            origin      = join(rawDataFolder, subFileList[i])
            destination = join(tsFolder, subFileList[i])
            os.system(f"cp {origin} {destination}")
        else:
            origin      = join(rawDataFolder, subFileList[i])
            destination = join(trFolder, subFileList[i])
            os.system(f"cp {origin} {destination}")

