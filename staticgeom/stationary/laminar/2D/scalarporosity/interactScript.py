#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 14:26:33 2023

@author: al
"""

from modules.alya_simulate import get_mesh, set_limits
from modules.alya_simulate import run_alya, run_case, run_case_battery


"""
Usage examples
"""
# Run the case 'matrix' inside 'matrix' folder with the following parameters
#   ang = 30                30 degrees of inclination
#   velOrd = 0              v = 10**0 = 1
#   poro =                  No porosity defined
#
#runCase("matrix", "matrix", 30, 0)

# Run the case 'matSurr' inside 'surrogate' folder with the following parameters
#   ang = 30                30 degrees of inclination
#   velOrd = 0              v = 10**0 = 1
#   poro = 100
#
#runCase("surrogate", "matSurr", 30, 0, 100)

# Set the limits for a battery of runs with the following values:
#   ang1 = 0 degrees        ang2 = 45 degrees
#   vel1 = 0.01 = 10**(-2)  -> velOrd1 = -2
#   vel2 = 1    = 10**(0)   -> velOrd2 = 0
#
#setLimits(0, 45, -2, 0)

# Run a battery of different velocity conditions (nAng and nVel) for the case
# 'matSurr' inside 'surrogate' folder. The limits for angles and velocity modulus
# are set with 'setLimits()'
#   nAng = 5
#   nVel = 10
#   poro = 300
#runCaseBattery("surrogate", "matSurr", 5, 10, poro=300)


#runCase("surrogate", "surr", 5, 0.1, [10, 100, 1000, 10000], True, True)