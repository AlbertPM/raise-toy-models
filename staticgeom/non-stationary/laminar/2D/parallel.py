#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 10 18:28:05 2022

@author: al
"""
import mpi4py
mpi4py.rc.thread_level = 'single'

from mpi4py import MPI
import numpy as np


import sys
from os import listdir
import time


from modules.mpi_functions import mpi_end_workers, mpi_worker_find_poroL, \
                                  mpi_worker2folder, mpi_init_workers, \
                                  get_shared_win_int, get_shared_win_float

from modules.mpi_datatypes import mpi_dtype_int, np_dtype_int
from modules.mpi_datatypes import mpi_dtype_float, np_dtype_float

import modules.parallel_params as pp

from modules.parallel_manager_functions import  update_completion_folder, \
                                                find_idle_process, \
                                                find_folder_to_poro, \
                                                find_folder_to_sim, \
                                                count_simulated_cases, \
                                                rm_temp_folders, \
                                                exists_saved_porosities, \
                                                read_porosity_limits, \
                                                save_porosity_limits
                                                
from modules.parallel_worker_functions import get_poro_lim_values, \
                                              run_conti_case, \
                                              run_find_max_poro
    

import subprocess

#%%


"""
function definitions
"""

#%%


if __name__ == "__main__":

    import argparse

    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--folder", type=str, required=True,
    	help="Name of the folder with the model")
    ap.add_argument("-c", "--case", type=str, required=True,
    	help="Name of the case to simulate")
    ap.add_argument("-N", "--Num", type=int, required=True,
        help="Max number of total simulations to run")
    ap.add_argument("-hr", "--highRes", action='store_true',
        help="Mark whether current simulations are for high resolution model")
    ap.add_argument("-tf", "--tempfolder", type=str, required=False,
        help="Location of temporal disk storage (defaults to . if unset)")
    args = vars(ap.parse_args())


    # name of folder with the case to simulate in Alya
    caseFolder = args["folder"]
    # name of the case to simulate in Alya
    caseName = args["case"]
    # temporal folder
    if args["tempfolder"] == None:
        tempFolder = "."
    else:
        tempFolder = args["tempfolder"]

    
    # MPI communication between python processes dims and rank
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    
    # number of threads running Alya
    nWorkers = size - 1
    # number of folders with split run conditions
    nFolders = pp.nAVCond
    # number of simulations per folder
    nSim_Folder = args["Num"] // nFolders


    # Print MPI processes and folders and check Number of simulations to de done
    if rank == 0:
        print(f"Number of workers: {nWorkers}")
        print(f"Number of folders: {nFolders}")
        print(f"Number of sims per folder: {nSim_Folder}")
        
    # Check that there are more run folders than processes for them
    if ( nWorkers > nFolders ):
        if rank == 0:
            print("[ERROR]: Too many processes", file=sys.stderr)
        exit()

    # Check that there is at least 1 simulation per folder
    if ( nSim_Folder < 1 ):
        if rank == 0:
            print("[ERROR]: No simulations to be run, increase number", file=sys.stderr)
        exit()     


    # Creation of shared memory window between processes
    # for process status tracking -> window of 'nWorkers' integers
    win_prcStatus = get_shared_win_int(comm, rank, nWorkers)    
    
    # Creation of shared memory window between processes
    # for Porosity limit stored values -> window of 2x'nFolders' floats
    win_poroLimits = get_shared_win_float(comm, rank, 2*nFolders)



    # Creation of control lists in manager process to track:
    # Status of workers                 -> prcStatus[nWorkers]
    # cases simulated at each folder    -> foldList[nFolders]
    # folder completion status          -> notCompleted[nFolders]
    # folder locking status             -> foldLocked[nFolders]
    # process assigned to folders       -> procOnFolder[nFolders]
    # porosity limit found status       -> poroLimitFound[nFolders]
    # porosity values                   -> poroValues[nFolders]
    if ( rank == 0 ):
        # List with process status from window allocated memory for MPI
        # 0 -> Worker not contacted
        # 1 -> Idle
        # 2 -> Working    
        # Initialize the array to 0 (not contacted)
        prcStatus = np.frombuffer(win_prcStatus, dtype=np_dtype_int)
        prcStatus[:] = np.zeros(len(prcStatus), dtype=np_dtype_int)
        
        # List with numbers of cases completed per each folder
        foldList = [0] * nFolders
        # Count completed cases per folder from 'results' folder
        for i in range(nFolders):
            foldList[i] = count_simulated_cases("./results", f"{caseFolder}-{i}")
        
        # List with no completion status for each folder to simulate
        notCompleted = [True] * nFolders
        # List with folder status
        foldLocked = [False] * nFolders
        # List with index of process assigned to each folder
        procOnFolder = [0] * nFolders
        
        # Check partial completion status in 'results' folder
        update_completion_folder(comm, caseFolder, foldList, nSim_Folder, 
                                 notCompleted, foldLocked)
        
        # Fill porosity related arrays
        # poroLimitFound = np.frombuffer(win_poroStatus, dtype=np_dtype_bool)
        poroLimitsFound = [args["highRes"]] * nFolders
        poroLimits = np.frombuffer(win_poroLimits, dtype=np_dtype_float)
        poroLimits[:] = np.zeros(len(poroLimits), dtype=np_dtype_float)
        # Read porosity limits if saved file exists
        pLimitsSaved = exists_saved_porosities("porosities.bin")
        if not args["highRes"] and pLimitsSaved:
            poroLimits[:] = read_porosity_limits(pp.nAbins, 
                                                 pp.nVbins, 
                                                 "porosities.bin")
            print(f"Porosities loaded: {poroLimits}")
        elif not args["highRes"]:
            print("No porosity limits file to load")
        else:
            pLimitsSaved = True

    
    comm.Barrier()
    
    
    # worker initialization (Manager and Worker)
    mpi_init_workers(comm, rank, win_prcStatus)
    

    tPrint0 = time.time()
    # Main loop for manager process
    if ( rank == 0 ):
    
        tstep = [0] * nWorkers
        # Main loop for master process
        while any(notCompleted):
            
            # Select an Idle Worker
            proc = find_idle_process(prcStatus, procOnFolder)
            
            # Save porosity limits when all have been found
            if not(pLimitsSaved) and all(poroLimitsFound):
                save_porosity_limits(pp.nAbins, pp.nVbins, 
                                     poroLimits, "porosities.bin")
                pLimitsSaved = True
            
            # Try to assign IDLE workers to a task
            if proc != 0:
                # Check for porosity limits still to be found
                # Select a not locked Folder where porosity limit is still not found
                foldIdx = find_folder_to_poro(poroLimitsFound, foldLocked)
                if foldIdx != -1:
                    # change process status to working
                    prcStatus[proc-1] = 2
                    # Lock the same velocity folders to other processes
                    vId = foldIdx % pp.nVbins
                    for i in range(pp.nAbins):
                        foldLocked[i * pp.nVbins + vId] = True
                    # get process to look for porosity limit
                    mpi_worker_find_poroL(comm, proc, foldIdx)
                    # store that proc is assigned to folder
                    procOnFolder[foldIdx] = proc
                    # Print information
                    msg = f"Process {proc} looking for porosity limit on folder {foldIdx}"
                    t = time.time()
                    print(f"<< {int(t-tPrint0)} s >>: {msg}")
                    continue
            
                # Assign Idle Worker to folder       
                # Select a Folder to run Simulation (poro limit has had to be found)
                foldIdx = find_folder_to_sim(foldList, poroLimitsFound, foldLocked)
                # Start a process in a folder if both are available
                if foldIdx != -1:
                    # change process status to working
                    prcStatus[proc-1] = 2
                    # Lock the folder to other processes
                    foldLocked[foldIdx] = True
                    # start process simulation in folder
                    mpi_worker2folder(comm, proc, foldList[foldIdx]+1, foldIdx)
                    # store that proc is assigned to folder
                    procOnFolder[foldIdx] = proc
                    # Print information
                    msg = f"Process {proc} simulating on folder {foldIdx}"
                    t = time.time()
                    print(f"<< {int(t-tPrint0)} s >>: {msg}")
                    continue
            
            # Lock process status window
            win_prcStatus.Lock(rank = 0, lock_type=MPI.LOCK_EXCLUSIVE)
    
            # look at each process status
            for i, wStatus in enumerate(prcStatus):
                
                pr = i+1        # process number
                
                # if process is not idle, but not assigned to folder
                if (wStatus != 1 and not (pr in procOnFolder) ):
                    msg = f"[ERROR] Process {pr} not on any folder"
                    t = time.time()
                    print(f"<< {int(t-tPrint0)} s >>: {msg}", file=sys.stderr)
                    comm.Abort()
                
                # if process is idle but attached to a folder...
                elif ( wStatus == 1 and (pr in procOnFolder) ):
                    # ...get the folder index it is attached to...
                    foldIdx = procOnFolder.index(pr)
                    # ...Get former simulated cases at that folder...
                    prevSimul = foldList[foldIdx]
                    # ...Update the total number of cases simulated at that folder...
                    foldList[foldIdx] = count_simulated_cases("./results", f"{caseFolder}-{foldIdx}")
                    # Check whether process was finding porosity limit
                    if (prevSimul == foldList[foldIdx]):
                        # receive porosity value from process pr
                        inBuf = np.zeros(2, dtype=np_dtype_float)
                        comm.Recv([inBuf, mpi_dtype_float], source=pr, tag=0)
                        # DEBUG INFO -----
                        # print(f"pr: {pr} | inBuf = {inBuf}")
                        # -----
                        # ...unlock the same velocity folders to other processes
                        # and store the porosity values values found
                        win_poroLimits.Lock(rank=0)
                        for i in range(pp.nAbins):
                            j = i * pp.nVbins + foldIdx % pp.nVbins
                            poroLimits[2*j:2*j+2] = inBuf[:]
                            poroLimitsFound[j] = True
                            foldLocked[j] = False
                        win_poroLimits.Unlock(rank=0)
                        msg = ( f"Process {pr} FOUND PORO LIMITS on folder {foldIdx}" +
                                f" P = {poroLimits[2*foldIdx:2*foldIdx+2]}" )
                    else:
                        msg = ( f"Process {pr} FINISHED on folder {foldIdx} " +
                                f"[{foldList[foldIdx]} / {nSim_Folder}]" )
                        # ...unlock the folder for another process...
                        foldLocked[foldIdx] = False
                    t = time.time()
                    print(f"<< {int(t-tPrint0)} s >>: {msg}")
                    # ...set the process on that folder to manager...
                    procOnFolder[foldIdx] = 0
                    
                # if process is idle but not attached to a folder
                elif ( wStatus == 1 and not (pr in procOnFolder) ):
                    t = time.time()
                    tint = int(t // 60)
                    if tint > tstep[i]:
                        msg = f"Process {pr} is IDLE"    
                        print(f"<< {int(t-tPrint0)} s >>: {msg}")
                        tstep[i] = tint        
            
            # Unlock process status window
            win_prcStatus.Unlock(rank = 0)
            
            # update status of completion for the folders
            update_completion_folder(comm, caseFolder, foldList, nSim_Folder, 
                                     notCompleted, foldLocked)
        
        # clean temporary folders
        rm_temp_folders(tempFolder, caseFolder, nFolders)
        
        # Send close signal to all workers
        mpi_end_workers(comm, nWorkers)
        
        t = time.time()
        msg = "MAIN PROCESS ENDED"    
        print(f"<< {int(t-tPrint0)} s >>: {msg}")
    
    # Main loop for worker processes
    else:
        while True:
            # status for communication
            status = MPI.Status()
            # tag=1 to order worker to find poro limit
            # tag=2 to order worker to start simulation
            # tag=3 to order the worker to terminate
            inBuf = np.zeros(2, dtype=np_dtype_int)
            comm.Recv([inBuf, mpi_dtype_int], source=0, tag=MPI.ANY_TAG, status=status)
            foldIdx, runNum = (inBuf[0], inBuf[1])
            # look for porosity limit
            if status.tag == 1:
                # look for 'poroLimit'
                pmin, pmax = run_find_max_poro(tempFolder, caseFolder, caseName, foldIdx)
                # Send porosity value to manager process
                outBuf = np.array([pmin, pmax], np_dtype_float)
                req = comm.Isend([outBuf, mpi_dtype_float], dest=0, tag=0)
                # Set the worker as Idle after completion
                buf = np.ones(1, dtype=np_dtype_int)
                win_prcStatus.Lock(rank=0)
                win_prcStatus.Put(buf, target_rank=0, target=(rank-1, 1, mpi_dtype_int))
                win_prcStatus.Unlock(rank=0)
                req.wait()
            # run simulation
            elif status.tag == 2:
                pLims = get_poro_lim_values(win_poroLimits, foldIdx)
                # DEBUG INFO -----
                # print(f"pr = {rank} | pLims = {pLims}")
                # -----
                # run simulation 'runNum' in folder index 'foldIdx' 
                run_conti_case(nSim_Folder, tempFolder, caseFolder, caseName, 
                               foldIdx, pLims, runNum, args["highRes"])
                # Set the worker as Idle after completion
                buf = np.ones(1, dtype=np_dtype_int)
                win_prcStatus.Lock(rank=0)
                win_prcStatus.Put(buf, target_rank=0, target=(rank-1, 1, mpi_dtype_int))
                win_prcStatus.Unlock(rank=0)
            # Terminate worker
            elif status.tag == 3:
                msg = f"Process {rank} TERMINATING"
                t = time.time()
                print(f"<< {int(t-tPrint0)} s >>: {msg}")
                break
