#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 19 15:26:54 2023

@author: al
"""


import subprocess
from os.path import exists
import numpy as np
from scipy.stats import norm
from scipy.optimize import curve_fit
from scipy.special import ndtri


import parallel_params as pp


from mpi_datatypes import mpi_dtype_float, np_dtype_float


from alya_simulate import run_case, clean_outputs, get_mesh, \
                          get_downstream_V_field

from alya_processing import check_alya_convergence

from parallel_porosity import get_random_porosity, get_theory_poro_limits


# Create a numpy random generator using as seed the text on "string"
# Used to guarantee repeatability on the simulations
def set_seed_for_folder(string):
    seed = int.from_bytes(f"{string}".encode(), 'big')
    return np.random.default_rng(seed)



# create a copy of the folder to run the simulation there in parallel
# without interfering with other processes
def create_temp_folder(fold, newFold):
    if (not exists(newFold)):
        subprocess.run(["cp", "-r", f"./{fold}", newFold])



# Returns the limits used to get a random angle and velocity for 
# each folder
def get_limits_for_folder(aLims, vLims, nfold):
    nV = len(vLims) - 1
    
    # Folders will be split first on angles and then on velocity
    aIdx = nfold // nV
    vIdx = nfold % nV
    
    return aLims[aIdx:aIdx+2], vLims[vIdx:vIdx+2]



# Return the porosity value limits for the folder with index idxFolder
def get_poro_lim_values(win, idxFolder):
    buf = np.zeros(2, dtype=np_dtype_float)
    win.Lock(rank=0)
    win.Get(buf, target_rank=0, target=(2*idxFolder, 2, mpi_dtype_float))
    win.Unlock(rank=0)
    return [float(buf[0]), float(buf[1])]


# Obtain random values for angle and modulus of velocity and surrogate
# porosity for the  
def get_vars_conti(nSF, aL, vL, pL, rng, nC, hR):    
    # get random uniform distributed values of angles and velocity inside
    # local limits (global velocity is not uniform)
    ang = rng.uniform(aL[0], aL[1], nSF)[nC-1]
    vel = rng.uniform(vL[0], vL[1], nSF)[nC-1]
    # get porosity value from calculated limits for that folder based
    # modify porosity to 0 if high resolution cases are simulated
    if hR:
        por =[0,0,0,0]
    else:
        por = get_random_porosity(nSF, pL, rng, nC)
    
    return (ang, vel, por)
    
    return (ang, vel, por)


# Run a case in a folder using the continuation feature from Alya to start
# from a previously solved simulation.
# nSF       -> Number of simulations per folder
# tempF     -> Pathname of the temporal folder where simulations will run
# fold      -> Name of the folder with data for the simulation
# case      -> Base name of the files with data for the simulation
# nF        -> Index of the folder where a parallel simulation is run
# pL        -> Array with lower and upper limits of porosity for that folder
# nC        -> Number of simulation being run for that folder
# hR        -> Flag to define whether the simulation is for a high Res model
def run_conti_case(nSF, tempF, fold, case, nF, pL, nC, hR = False):
    # create a copy of the folder to run the simulation there in parallel
    # without interfering with other processes (in case it doesn't exist yet)
    newFold = f"{tempF}/{fold}-{nF}"
    create_temp_folder(fold, newFold)
    
    # Set random seed for that particular folder
    rng = set_seed_for_folder(f"{fold}-{nF}")
    
    # Get random angle, velocity and porosity
    #   get angle and velocity limits for the used folder idx
    aBin, vBin = get_limits_for_folder(pp.aBinsLims, pp.vBinsLims, nF)
    #   get particular values for angle, velocity and porosities
    a, v, p = get_vars_conti(nSF, aBin, vBin, pL, rng, nC, hR)
    
    # run the case with ang and vel values
    run_case(newFold, case, a, v, p, False, HPC=True, tSteps=pp.timeSteps)

    cvgFile = f"{newFold}/{case}.nsi.cvg"
    if (not check_alya_convergence(cvgFile, v)):
        svF = f"./results/{fold}-{nF}-{a}-{v}-{p[0]}x{p[1]}x{p[2]}x{p[3]}.cvg"
        subprocess.run(["cp", cvgFile, svF])
    
    # clean output from simulation for the next one but keep necessary data
    #   for restarting the simulation with the previous values
    clean_outputs(newFold, case)



# Get the average of L2 diff between a vector fields and a reference constant
# vector field with velocity mod v and argument a
def field_diff(VF, v, a):
    arad = a * np.pi / 180
    VRef = np.array([v * np.cos(arad), v * np.sin(arad)]) * np.ones_like(VF)
    Vdif2 = (VF - VRef)**2
    Vdifnorm = np.sqrt(np.sum(Vdif2, 2))
    return np.average(Vdifnorm) # / np.average(V0norm)


    
# Find the maximum porosity to simulate at each folder conditions (v and ang)
# tempF     -> Pathname of the temporal folder where simulations will run
# fold      -> Name of the folder with data for the simulation
# case      -> Base name of the files with data for the simulation
# nF        -> Index of the folder where a parallel simulation is run
def run_find_max_poro(tempF, fold, case, nF):
    
    def propose_new_limits(xarr, yarr, xsol0):
        # Try to fit points with a normal cdf
        fNorm = lambda x_f,mu,sigma, M: M*norm(mu,sigma).cdf(x_f)
        mu, sigma, M = curve_fit(fNorm, xarr, yarr)[0]       
        # Get the points where CDF is 0.01 and 0.99
        xl = ndtri(0.005) * sigma + mu
        xr = ndtri(0.995) * sigma + mu
        
        return np.array([xl, xr])

    def adjust_new_points(pLN, lP):
        N = len(lp)
        pNew = []
        for pL in pLN:
            idx = np.searchsorted(lP, pL)
            if idx == 0:
                pNew.append(1.5*lP[0]-0.5*lP[1])
            elif idx == N:
                pNew.append(1.5*lP[N-1]-0.5*lP[N-2])
            else:
                pNew.append(0.5*(lP[idx]+lP[idx-1]))
        
        return np.array(pNew)
    
    # create a copy of the folder to run the simulation there in parallel
    # without interfering with other processes
    newFold = f"{tempF}/{fold}-{nF}"
    create_temp_folder(fold, newFold)

    # get the mesh related files without output on stdout
    get_mesh(newFold, case, False)  
    
    # Get angle and velocity
    #   get angle and velocity limits for the used folder idx
    #   and set the average angle and velocity for that folder
    aBin, vBin = get_limits_for_folder(pp.aBinsLims, pp.vBinsLims, nF)
    a = sum(aBin) / 2
    v = sum(vBin) / 2
    
    # Initially defined porosity limits based on theoretical order of magnitude
    ptheo = get_theory_poro_limits(v)    

    # Pass to log and expand for derivatives search
    lp = np.log10(ptheo)
    varlp = lp[1]-lp[0]
    lp = np.array([lp[0]-varlp/6, lp[0], lp[0]+varlp/3,
                   lp[1]-varlp/3, lp[1], lp[1]+varlp/6])
    
    # Index of starting proposed porosity limits
    pIdx = [1, 4]
    
    # Simulate and obtain normalized velocity field average variation (NVFAV)
    DV = []         # List of NVFAV values
    # Populate DV list
    for p in list(10**lp):
        # Get downstream velocity field for a, v and p conditions
        VF = get_downstream_V_field(newFold, case, a, v, float(p))
        # Add NVFAV value against homogeneous field with v and a
        DV.append(field_diff(VF, v, a))
    DV = np.array(DV)
    
    pLim = np.array([lp[pIdx[0]], lp[pIdx[1]]])
    # Flag for porosity found
    notPoroLimitsFound = True                                   
    while notPoroLimitsFound:
        # Propose new points as the porosity limits
        pLimOld = pLim
        pLim = propose_new_limits(lp, DV, pLimOld)
        # Check convergence
        pRange0 = pLimOld[1] - pLimOld[0]
        pRange = pLim[1] - pLim[0]
        # DEBUG INFO -----
        # print(f"v = {v} | {len(lp)}")
        # print(f"v = {v} | porL = {pLim[0]}, {pLim[1]}")
        # print(f"v = {v} | old Range = {pRange0}, new Range = {pRange}, "+
        #       f"diff = {pRange - pRange0}")
        # -----
        if ( abs(pRange - pRange0) < 1e-2 * max(pRange, pRange0) or
             len(lp) >= 10 ):
            notPoroLimitsFound = False
            next
        # find 2 new points to simulate
        pNew = adjust_new_points(pLim, lp)
        # Simulate for new points NVFAV
        newDV = []
        for p in list(10**pNew):
            VF = get_downstream_V_field(newFold, case, a, v, float(p))
            newDV.append(field_diff(VF, v, a))
        newDV = np.array(newDV)
        # Insert new points in existing arrays
        insertIdx = np.searchsorted(lp, pNew)
        lp = np.insert(lp, insertIdx, pNew)
        DV = np.insert(DV, insertIdx, newDV)
    
    return float(10**pLim[0]), float(10**pLim[1])