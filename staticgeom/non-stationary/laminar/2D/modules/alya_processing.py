#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 13:16:11 2023

@author: al

Module with classes and functions to process simulation results once finished


Auxiliary classes for data management:
---
class point
class node
class scNode


Class that reads the velocity and pressure components of a rectangular
witness mesh in ALYA and stores them for further processing
---
class witnessPointsRect:

    
Function that takes the data inside a a witnessPointsRect class and appends
it to an open file with descriptor fDesc
---
def write_wp_rect(wp, fDesc):

    
Function that processes the upstream and downstream witness mesh information
from a simulation and stores the result in a file inside the results folder.
Creates the folder if it doesn't exists
---
def write_results(fold, case, ang, vel, por):

    
Function that checks the convergence of the simulation from the .nsi.cvg file
against the reference input velocity
---
def check_alya_convergence(fName, v):

"""


import sys
import numpy as np
import glob
import subprocess
import os.path


"""
Point and node class definitions
--------------------------------
"""

# 2D point definition.
class point:
    def __init__(self, pX, pY):
        self.x = pX
        self.y = pY

# Node definition
class node:
    def __init__(self, num, point):
        self.N = num
        self.p = point

#Scalar Node definition
class scNode:
    def __init__(self, num, esc):
        self.N = num
        self.s = esc

#%%

"""
Witness Points data processing
------------------------------
"""


"""
Class that reads witness points from a rectangular mesh on files after being
processed with the alya2pos script
The result is stored in an object with the data structure:
    .nodeNum            -> Number of nodes in the witness mesh
    .geoNodes[N]
        .node           -> Index of the node N in the mesh
        .p.x  .p.y      -> Coordinates for the node N
    .velNodes[N]
        .node           -> Index of the node N in the mesh
        .p.x  .p.y      -> Components of the velocity vector in the node N
"""
class witnessPointsRect:   

    # Simulations are defined in ./folder/case.* files    
    # Inside the 'folder', there is a directory with the name of the mesh
    # (./folder/wpMeshName) where the required files will be found.
    # By default the last step of the run is analyzed (nstep used to select
    # among different saved steps in case they are present.
    def __init__(self, folder, case, wpMeshName, tSteps=1):
        self.wpName = wpMeshName
        self.nodeNum = 0
        self.geoNodes = []      # List to store the geometry information for wp nodes
        self.velNodes = []      # List to store the velocity field for wp nodes
        self.preNodes = []      # List to store the pressure field for wp nodes
        self.timeValues = []    # List to store the time for different time steps
        
        # Read the time values data for the witness points captured
        name = f"{folder}/{wpMeshName}/{case}-{wpMeshName}.ensi.case"
        availableTimes = self.get_time_data(name)
        if tSteps > availableTimes:
            sys.exit("[ERROR]: Requesting more time steps than availabe "+
                      "simulation output files")
        else:
            self.timeValues = self.timeValues[-tSteps:]
        
        # Read the geometry data for the witness points captured
        name = f"{folder}/{wpMeshName}/{case}-{wpMeshName}.ensi.geo"
        self.get_geo_data(name)       # Function to get the data from the file
        
        # Read the velocity field data from the witness points
        name = f"{folder}/{wpMeshName}/{case}-{wpMeshName}.ensi.VELOC*"
        # only the nstep final steps are taken into account from all the possible captures
        for i in range(-tSteps, 0):
            namestep = glob.glob(name)[i]
            self.get_vel_data(namestep)       # Function to get the data from the file
        
        # Read the pressure field data from the witness points
        name = f"{folder}/{wpMeshName}/{case}-{wpMeshName}.ensi.PRESS*"
        # only the nstep final steps are taken into account from all the possible captures
        for i in range(-tSteps, 0):
            namestep = glob.glob(name)[i]
            self.get_pre_data(namestep)       # Function to get the data from the file

    # Get the time values from the .ensi.case file
    def get_time_data(self, fName):
        with open(fName, "r") as f:         # Open file for reading (and close)
            lines = f.readlines()
        
        readPhase = 0               # Set initial reading phase
        # Read the different lines and store the values accordingly
        for line in lines:
            if (readPhase == 0):                # Skip lines till "time values:" tag
                if (-1 != line.find("time values:")): readPhase = 1
            elif (readPhase == 1):              # Get the total coordinates number
                textvalues = line.split()
                for value in textvalues:
                    self.timeValues.append(float(value))
        if (readPhase != 1):
            sys.exit("[ERROR]: Time values data couldn't be correctly read")
        else:
            return len(self.timeValues)

    # Get the location of the points in the witness mesh
    def get_geo_data(self, fName):
        with open(fName, "r") as f:         # Open file for reading (and close)
            lines = f.readlines()
            
        readPhase = 0               # Set initial reading phase
        self.nodeNum = 0            # Initialize variables to get
        nodeList = []
        nodeX = []
        nodeY = []
        # Read the different lines and store the values accordingly
        for line in lines:
            if (readPhase == 0):                # Skip lines till "coordinates" tag
                if (-1 != line.find("coordinates")): readPhase = 1
            elif (readPhase == 1):              # Get the total coordinates number
                self.nodeNum = int(line)
                readPhase = 2
            elif (readPhase == 2):              # Get node numbers 
                nodeList.append(int(line))                     
                if ( self.nodeNum == len(nodeList) ): readPhase = 3
            elif (readPhase == 3):              # Get nodes X positions
                nodeX.append(float(line))
                if ( self.nodeNum == len(nodeX) ): readPhase = 4
            elif (readPhase == 4):              # Get nodes Y positions
                nodeY.append(float(line))
                if ( self.nodeNum == len(nodeY) ): readPhase = 5

        if (readPhase != 5):
            sys.exit("[ERROR]: Geo Data couldn't be correctly read")
        else:
            # Add a node class object to 'geoNodes' for each node read
            for i in range(self.nodeNum):
                self.geoNodes.append( node(nodeList[i], point(nodeX[i], nodeY[i])) )

    # Get the components X, Y of the velocity vectors on the witness point grid
    def get_vel_data(self, fName):
        with open(fName, "r") as f:         # Open file for reading (and close)
            lines = f.readlines()

        readPhase = 0                   # Set initial reading phase
        velX = []                       # Initialize variables to get
        velY = []
        # Read the different lines and store the values accordingly
        for line in lines:
            if (readPhase == 0):                # Skip lines till "coordinates" tag
                if (-1 != line.find("coordinates")): readPhase = 1
            elif (readPhase == 1):              # Get the X component of the velocity
                velX.append(float(line))
                if ( self.nodeNum == len(velX) ): readPhase = 2
            elif (readPhase == 2):              # Get the Y component of the velocity
                velY.append(float(line))
                if ( self.nodeNum == len(velY) ): readPhase = 3

        if (readPhase != 3):
            sys.exit("[ERROR]: Vel Data couldn't be correctly read")
        else:
            # Add a node class object to 'velNodes' for each node read
            self.velNodes.append([])
            for i in range(self.nodeNum):
                self.velNodes[-1].append( node(self.geoNodes[i].N, point(velX[i], velY[i])) )

    # Get the scalar value of the pressure on the witness point grid
    def get_pre_data(self, fName):
        with open(fName, "r") as f:         # Open file for reading (and close)
            lines = f.readlines()

        readPhase = 0                   # Set initial reading phase
        p = []                          # Initialize variables to get
        
        # Read the different lines and store the values accordingly
        for line in lines:
            if (readPhase == 0):                # Skip lines till "coordinates" tag
                if (-1 != line.find("coordinates")): readPhase = 1
            elif (readPhase == 1):              # Get the pressure
                p.append(float(line))
                if ( self.nodeNum == len(p) ): readPhase = 2

        if (readPhase != 2):
            sys.exit("[ERROR]: Vel Data couldn't be correctly read")
        else:
            # Add a node class object to 'velNodes' for each node read
            self.preNodes.append([])
            for i in range(self.nodeNum):
                self.preNodes[-1].append( scNode(self.geoNodes[i].N, p[i]) )

"""
Function that appends the data inside a 'wp' witnessPointRect object to an 
open file with file descriptor 'fDesc'.
Time steps information is stored at the start of each UpStream/DownStream block
preceded by the header "Time Steps Values"
The bulk of the data is stored next 
Stored as lines of the tab separated data in the form:
    CoordX      CoordY      VelCompX        VelCompY    Pressure
with an initial line header.
"""
def write_wp_rect(wp, fDesc, tHeader = False):
    
    # Function to get unique elements in a list
    def get_unique(origList):
        orderedList = [x for x in set(origList)]
        orderedList.sort(key=lambda x: float(x))
        return orderedList
    
    # Add header for time steps values
    tSteps = len(wp.timeValues)
    if tHeader:
        line = f"Time Steps Values:     {tSteps}\n"
        fDesc.write(line)
        # Write the different time values in the next line
        line = f"{wp.timeValues[0]}"
        for iT in wp.timeValues[1:]:
            line = line + f"\t{iT}"
        line = line + "\n"
        fDesc.write(line)
    
    # Add tag descriptor for Upstream/Downstream values
    if (wp.wpName == "UPW"):
        line = "UPSTREAM FIELDS\n"
    if (wp.wpName == "DOWNW"):
        line = "DOWNSTREAM FIELDS\n"
    fDesc.write(line)
    
    # X and Y unique coordinates listed
    nNodes = wp.nodeNum
    Xlist = get_unique([wp.geoNodes[i].p.x for i in range(nNodes)])     
    Ylist = get_unique([wp.geoNodes[i].p.y for i in range(nNodes)])
    # number of X and Y coordinates
    nX = len(Xlist)                                 
    nY = len(Ylist)
    
    if nX * nY != nNodes:
        sys.exit(f"[ERROR]: {nX}x{nY} does not match {nNodes} number of nodes")
    
    # Vmatrix creation and initialization to 0 (elements are vectors of dim 2)
    Vmatrix = np.zeros([tSteps, nX, nY, 2])  
    Pmatrix = np.zeros([tSteps, nX, nY])        # same for P with dimension 1       
    # Store V vector in corresponding matrix coordinates
    for t in range(tSteps):
        for n in range(nNodes):
            # get index value for node n from geoNodes field in wp
            iX = Xlist.index(wp.geoNodes[n].p.x)
            iY = Ylist.index(wp.geoNodes[n].p.y)
            # Vx and Vy assignation
            Vmatrix[t][iX][iY][:] = [wp.velNodes[t][n].p.x, 
                                     wp.velNodes[t][n].p.y]  
            # Pressure assignation
            Pmatrix[t][iX][iY] = wp.preNodes[t][n].s
        
    # writes a line for each velocity vector and pressure captured 
    # at each coordinate for different times
    for iX in range(nX):
        for iY in range(nY):
            cX, cY  = (str(Xlist[iX]), str(Ylist[iY]))
            line = f"{cX}\t{cY}"
            for t in range(tSteps):
                vX, vY  = (str(Vmatrix[t][iX][iY][0]), str(Vmatrix[t][iX][iY][1]))
                P       = str(Pmatrix[t][iX][iY])
                line = line + f"\t{vX}\t{vY}\t{P}"
            line = line + "\n"
            fDesc.write(line)    



# Process and write the output WP meshes from alya run to './results' folder      
def write_results(fold, caseN, ang, vel, por, tSteps = 1):
    if ( not os.path.exists("./results") ):     # Create results folder
        subprocess.run(["mkdir", "results"])

    angStr = str(round(ang, 3))             # rotation angle as a string
    velStr = str(round(vel, 3))             # velocity as a string
    if (type(por) == list):                 # porosity as a string when tensor
        nP = len(por)
        porStr = str(round(por[0], 3))
        for i in range(1, nP):
            porStr = porStr + f"x{round(por[i], 3)}"  # separator for components is 'x'
    else:                                   # porosity as a string when scalar
        porStr = str(round(por, 3))             

    # filename: ./results/folder-ang_vel_por.txt
    # We first strip the last part of 'folder' to take away the temporal
    #   path component
    cleanFold = fold.split("/")[-1]
    fileName = f"./results/{cleanFold}-{angStr}_{velStr}_{porStr}.txt"
    
    # Write in the header of the file the detailed simulation information
    # and the legend for the following velocity fields
    with open(fileName, 'w') as f:
        # simulation details
        line = "SIMULATION DATA\n"
        f.write(line)
        line = (f"    FOLDER:   {cleanFold}\n"
                f"    CASE:     {caseN}\n"
                f"    ANGLE:    {ang}\n"
                f"    VELOCITY: {vel}\n"
                f"    POROSITY: {por}\n")
        f.write(line)
        # header for velocity fields
        line = ("FIELDS\n"
                "Coordinates (X, Y)\t\tVelocity (Vx, Vy)\t\tPressure (P)\n")
        f.write(line)
        # Write the velocity fields for the Upwind 'UPW' and Downwind 'DOWNW'
        for i, wpName in enumerate(["UPW", "DOWNW"]):
            if (i == 0):
                tH = True
            else:
                tH = False                 
            # Process the ouptut WP mesh files into the wp class
            wp = witnessPointsRect(fold, caseN, wpName, tSteps)
            write_wp_rect(wp, f, tH)   # Write data inside 'wp' to 'fileName'


# Check convergence status for a simulation with a reference input velocity 'v'
# in the convergence file 'fName' (case.nsi.cvg)
# Returns True in case the simulation is inside the convergence tolerances
# 'ssTol' and 'convTol', it returns False otherwise
def check_alya_convergence(fName, v):
    with open(fName, "r") as f:         # Open file for reading (and close)
        lines = f.readlines()

    # Max num of last steps to check for convergence of momentum and steady st.
    nSt = 10

    tsLast  = int(lines[-1].split()[0])
    tsStart = max(1, tsLast - nSt)
    nLines  = tsLast - tsStart + 1
    
    if (nLines < 2):
        return False
    
    # Read values to use for convergence checking from file:
    #   Max velocity and pressure (min and max) final value
    #   Momentum convergence on nSt final steps
    #   Steady State convergence on nSt final steps
    vMax    = float(lines[-1].split()[9])
    pMax    = abs(float(lines[-1].split()[25]))
    pMax    = max(pMax, float(lines[-1].split()[26]))
    MConv   = [float(lines[-nLines + i].split()[15]) for i in range(nLines)]
    StSConv = [float(lines[-nLines + i].split()[34]) for i in range(nLines)]

    # Check for velocity inside bounds
    if (vMax > 10 * v):
        return False
    
    # Check for pressure inside bounds
    rho = 1     # density
    if (pMax > 50 * 0.5 * rho * v**2):
        return False
    
    # Check for convergence and/or steady state tolerance
    ssTol = 1e-8
    convTol = 1e-10
    
    moVar = 0
    ssVar = 0
    for i in range(nLines - 1):
        moVar = moVar + (MConv[i+1] - MConv[i]) / (nLines - 1)
        ssVar = ssVar + (StSConv[i+1] - StSConv[i]) / (nLines - 1)

    convFinal = MConv[-1] + moVar
    StStFinal = StSConv[-1] + ssVar

    if ( StStFinal > 10 * ssTol or convFinal > 1e5 * convTol ):
        return False

    return True