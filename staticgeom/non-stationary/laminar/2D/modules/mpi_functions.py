#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 17:44:29 2023

@author: al
"""

import sys
import numpy as np
import os.path
from mpi4py import MPI

file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

from mpi_datatypes import mpi_dtype_int, mpi_size_int, np_dtype_int
from mpi_datatypes import mpi_dtype_float, mpi_size_float, np_dtype_float
from mpi_datatypes import mpi_dtype_bool, mpi_size_bool, np_dtype_bool


"""
Functions for creating MPI shared memory windows at the root process for all
the comm processes to access
"""
# Creation of shared memory window between processes of 'n' integers
def get_shared_win_int(comm, rank, n):
    win_size = n * mpi_size_int if rank == 0 else 0
    return MPI.Win.Allocate(size=win_size, disp_unit=mpi_size_int, comm=comm)
    
# Creation of shared memory window between processes of 'n' floats
def get_shared_win_float(comm, rank, n):
    win_size = n * mpi_size_float if rank == 0 else 0
    return MPI.Win.Allocate(size=win_size, disp_unit=mpi_size_float, comm=comm)


"""
Functions called by the manager communicator to manage the status of worker
processes
"""

# Function for worker initialization
# Puts worker status at 1 on window or fails and exits
def mpi_init_workers(comm, rank, win):
    if ( rank == 0 ):
        comm.Barrier()
        win.Lock(rank=0)
        win.Unlock(rank=0)
        buf = np.frombuffer(win, dtype=np_dtype_int)
        if ( not np.all( buf == 1 ) ):
            # Send close signal to all workers
            nWorkers = comm.Get_size() - 1
            mpi_end_workers(nWorkers)
            print("[ERROR]: Workers can't be initialized", file=sys.stderr)
    else:
        buf = np.ones(1, dtype=np_dtype_int)
        win.Lock(rank=0)
        win.Put(buf, target_rank=0, target=(rank-1, 1, mpi_dtype_int))
        win.Unlock(rank=0)        
        comm.Barrier()

# Function called from manager process to end 'nW' worker processes in the
# communicator 'comm'
# Sends a message with the list [-1, -1] (it doesn't matter the actual values)
# and tag 3, the real flag to be interpreted by workers as end signal
def mpi_end_workers(comm, nW):
    # List of communication requests
    reqL = []
    # Send signal to end to all worker processes
    outBuf = np.array([-1, -1], np_dtype_int)
    for i in range(nW):
        reqL.append(comm.Isend([outBuf, mpi_dtype_int], dest=i+1 , tag=3))
    # Wait for communication to be completed
    MPI.Request.waitall(reqL)


# Send MPI signal to worker with process 'prc' in communicator 'comm' to find
# the porosity limit at folder 'nFold'
def mpi_worker_find_poroL(comm, prc, nFold):
    outBuf = np.array([nFold, 0], np_dtype_int)
    comm.Send([outBuf, mpi_dtype_int], dest=prc, tag=1)
    
    
# Send MPI signal to worker 'prc' to run the simulation 'nRun' at folder 'nFold'
def mpi_worker2folder(comm, prc, nRun, nFold):
    outBuf = np.array([nFold, nRun], np_dtype_int)
    comm.Send([outBuf, mpi_dtype_int], dest=prc, tag=2)
    


