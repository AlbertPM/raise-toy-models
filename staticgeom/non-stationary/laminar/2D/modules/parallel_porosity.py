#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 17:38:31 2023

@author: al
"""

import sys
import numpy as np
import sympy as sp
import os.path


file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)


import parallel_params as pp



# Returns the maximum value for porosity range depending on simulation
# nominal velocity, fluid parameters and surrogate characteristic length
def get_theory_poro_limits(v):
    advO = v / pp.obstLen
    visO = pp.visc / pp.obstLen**2
    # set max range for porosity as 1e4 times the max contributor on N-S eq.
    return [min(advO, visO)/pp.multPorMax, pp.multPorMax * max(advO, visO)]


# Select the porosity distribution and return N random values from it between
# the limits defined in pL
def poro_dist(nSF, rng, N, dType, pL, nC):
    idx = (nC-1)*N
    pmin, pmax = (pL[0], pL[1])
    if (dType == 'uniform'):        # Uniform between pmin and pmax
        p = rng.uniform(pmin, pmax, N * nSF)[idx:idx+N]
    elif (dType == 'triangular'):   # Triangular between pmin and pmax
        p = rng.triangular(pmin, pmin, pmax, N * nSF)[idx:idx+N]
    elif (dType == 'log'):          # Logaritmic between pmin and pmax
        plog = rng.uniform(np.log(pmin), np.log(pmax), N * nSF)[idx:idx+N]
        p = np.exp(plog)
    else:
        print("[ERROR]: parallel_porosity.py/poro_dist ->"+
              "Bad porosity distribution defined")
        exit()
    return p


# Return a porosity matrix from a diagonal p1, p2 porosity matrix
# on a theta rotated basis
def get_rotated_matrix(p1, p2, theta):
    cosTh = np.cos(theta)
    sinTh = np.sin(theta)
    D1 = p1 * cosTh**2 + p2 * sinTh**2
    D2 = p1 * sinTh**2 + p2 * cosTh**2
    S  = (p1 - p2) * cosTh * sinTh
    return [D1, S, S, D2] 


# Get a matrix in canonical basis from a diagonal matrix in an
# eigenvector space corresponding to 
def get_changed_basis(p1, p2, theta1, theta2):
    # Get normalized eigenvectors expressed in normal basis
    eigenV = [ np.cos(theta1), np.sin(theta1) ]
    eigenW = [ np.cos(theta2), np.sin(theta2) ]
    # Get change of basis matrix from eigenvector basis to canonical
    S = sp.Matrix( [ [eigenV[0], eigenW[0]], [eigenV[1], eigenW[1]] ])
    # Get diagonal matrix in eigenvector basis
    D = sp.Matrix( [ [p1, 0], [0, p2]] )
    # Transform to canonical space
    M = S * D * S.inv
    return [ float(m) for m in [M[0,0], M[1,0], M[0,1], M[1,1]] ]


def get_random_porosity(nSF, pL, rng, nC):
    # Calculate porosity from random distributions as an scalar value or
    # 2 different creation process tensors
    if ( pp.poroType == 'scalar' ):
        por = float( poro_dist(nSF, rng, 1, pp.poroDistrib, pL, nC) )
    elif ( pp.poroType == 'rotBasis' ):
        # get 2 porosity ortogonal eigenvalues and 1 rotation angle
        por = list( poro_dist(nSF, rng, 2, pp.poroDistrib, pL, nC) )
        th = rng.uniform(-np.pi/2, np.pi/2, nSF)[nC-1]
        # Calculate rotated porosity matrix
        por = get_rotated_matrix(por[0], por[1], th)
    elif ( pp.poroType == 'changeBasis' ):
        # get 2 porosity ortogonal eigenvalues and
        # 2 eigenvector directions
        por = list( poro_dist(nSF, rng, 2, pp.poroDistrib, pL, nC) )
        th1 = rng.uniform(-np.pi/2, np.pi/2, nSF)[nC-1]
        th2 = rng.uniform(-np.pi/2, np.pi/2, nSF)[nC-1]
        # Calculate the change of basis
        por = get_changed_basis(por[0], por[1], th1, th2)
    else:
        print("[ERROR]: parallel_porosity.py/get_random_porosity ->"+
              "Bad porosity type defined")
        exit()
    
    return por