#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 19:17:11 2023

@author: al
"""

import sys
import numpy as np
import subprocess
import os.path
import glob
import io

file_dir = os.path.dirname(__file__)
sys.path.append(file_dir)

from alya_config import set_sim_vars
from alya_processing import write_results, witnessPointsRect, write_wp_rect


# Processes the file case.msh inside fold and prepares it for alya 

def get_mesh(fold, case, output):
    # Check for existence of file 'case.geo' and 'case.msh' inside of 'fold'
    geoFileExists = os.path.exists(fold + "/" + case + ".geo")
    mshFileExists = os.path.exists(fold + "/" + case + ".msh")
    if ( not geoFileExists and not mshFileExists ):
        # There is no geometry related files in the 'fold' directory
        sys.exit("No geometry necessary files inside " + fold)
    elif ( geoFileExists and not mshFileExists ):        
        # Creates file case.msh from case.geo using the program gmsh in 2D with format msh2
        print("Creating " + case + ".msh from " + case + ".geo")
        proc = subprocess.run(["gmsh", case + ".geo", "-2", "-format", "msh2"], 
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                              universal_newlines=True, cwd=fold)
        if output:
            print(proc.stdout)
            print(proc.stderr)
        mshFileExists = os.path.exists(fold + "/" + case + ".msh")

    if ( mshFileExists ):
        # process file case.msh into case.dims.dat, case.fix.bou, case.geo.dat
        proc = subprocess.run(["gmsh2alya.pl", case, "--bcs=boundaries", "--bulkcodes"], 
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                              universal_newlines=True, cwd=fold)
        if output:
            print(proc.stdout)
            print(proc.stderr)
    else:
        sys.exit(f"[ERROR]: File {case}.msh could not be found or generated")



# Run alya for case inside folder and postprocess the data obtained
def run_alya(fold, case, output=False, HPC=False):

    # HPC environment modifications
    if HPC:
        exe = "alya"         # Sequential execution
        flagNP = ""
        numProc = ""
        exe2 = ""
    else:
        exe = "mpirun"      # Parallel execution using 4 threads
        flagNP = "-np"
        numProc = "4"
        exe2 = "alya"

    # stdout for Alya is captured to avoid too much clogging unless 
    # specified otherwise
    executed = subprocess.run([exe, flagNP, numProc, exe2, case], 
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                              universal_newlines=True, cwd=fold)
    if output:
        print(executed.stdout)
        print(executed.stderr)

    # processes the ouput data from alya inside 'fold' 
    # and 'fold/UPW', 'fold/DOWNW'
    subprocess.run(["cp", "./" + case + ".post.alyadat", 
                    "./UPW/" + case + "-UPW.post.alyadat"], cwd=fold)
    subprocess.run(["cp", "./" + case + ".post.alyadat", 
                    "./DOWNW/" + case + "-DOWNW.post.alyadat"], cwd=fold)
    procs = []
    procs.append(subprocess.run(["alya2pos", case], 
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                universal_newlines=True, cwd=fold)
                 )
    procs.append(subprocess.run(["alya2pos", case + "-UPW"], 
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                universal_newlines=True, cwd=fold+"/UPW")
                 )
    procs.append(subprocess.run(["alya2pos", case + "-DOWNW"], 
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                universal_newlines=True, cwd=fold+"/DOWNW")
                 )

    if output:
        for p in procs:
            print(p.stdout)
            print(p.stderr)   
        


# Runs a 'case' inside 'fold' with the angle 'ang' (degrees) and velocity 
# modulus 'vel' indicated (passed as strings). The value 'poro' is passed as
# porosity for the surrogate cases where it applies (can be set as 0 by default)
def run_case(fold, case, ang, vel, poro=0, output=False, HPC=False, tSteps = 1):
    # Create the mesh if it doesn't exist yet
    get_mesh(fold, case, output)
    
    # Modify case.ker.dat to set values for velocity angle and modulus and porosity 
    set_sim_vars(fold, case, ang, vel, poro)
    # run alya and postprocess data     
    run_alya(fold, case, output, HPC)             
    # write captured data in './results'
    write_results(fold, case, ang, vel, poro, tSteps)     


# set the limits for angle (degrees), velocity (exponent of order of magnitude)
# and porosity (for the surrogate model simulations) that will be used when 
# running a case battery with 'runCaseBattery()'
def set_limits(ang1, ang2, vel1, vel2, por1=0, por2=0):
    global CASE_BATTERY_LIMITS
    CASE_BATTERY_LIMITS = [ang1, ang2, vel1, vel2, por1, por2]


# Runs a battery of alya simulations for 'case' inside 'fold' with 'nAng' angles between 
# 0 and 45 and 'nVel' velocity modulus between 0.01 and 0.1 by default, or the limits
# defined in the global variable 'Lims', set with 'setLimits()'.
# Writes the witnesspoints results in the results folder
def run_case_battery(fold, case, nAng, nVel, nPor=1):

    global CASE_BATTERY_LIMITS   
    # Check for defined limit values or set them by default
    # Velocity limits are defined by log10(v) (order of magnitude)    
    if (not 'CASE_BATTERY_LIMITS' in globals()):   Lims = [0, 45, -2, 0, 0, 0]
    print(CASE_BATTERY_LIMITS)
    Lims = CASE_BATTERY_LIMITS
    
    # Create lists for the variables (Angles and velocity order are trivial)
    angL        = np.linspace(Lims[0], Lims[1], nAng)
    velL        = np.linspace(Lims[2], Lims[3], nVel)
    # Porosity could include a 0, but the distribution is defined to be logaritmic.
    # A log2 based distribution from the last value is implemented.
    if (Lims[4] > 0):
        porL = list(10**np.linspace(np.log10(Lims[4]), np.log10(Lims[5]), nPor))
    else:
        exit("[ERROR]: Porosity defined with a negative value")
        
    ListVars    = list([i.flatten() for 
                        i in np.meshgrid(angL, velL, porL, indexing='ij')])

    # Iterate for all the possible variables combinations
    for n in range(nAng * nVel * nPor):
        
        # get values for angle, velocity and porosity to use
        ang, vel, por = (ListVars[0][n], ListVars[1][n], ListVars[2][n])
        angStr = str(round(ang, 5))             # rotation angle as a string
        velStr = str(round(vel, 5))             # velocity as a string
        porStr = str(round(por, 5))             # porosity as a string
        
        print("-----------")                    # print progress of the run
        print("run: " + angStr + " , " + velStr + " , " + porStr)
        print("completion: " + str( round( n/(1.0*nAng*nVel*nPor)*100, 0 ) ) + "%")
        print("-----------")
        
        run_case(fold, case, ang, vel, float(por))      # run the case with ang and vel values
        
        subprocess.run(["./clean.sh"], cwd=fold)    # clean the directory
        
        
# Clean output related files from the folder 'nFold' keeping the data needed
# to restart the simulation from last step
def clean_outputs(fold, case):
    for path in [f"{fold}/{case}", 
                 f"{fold}/UPW/{case}-UPW",
                 f"{fold}/DOWNW/{case}-DOWNW"]:
        for file in glob.glob(f"{path}-PRESS*.post.alyabin"):
            os.remove(file)
        for file in glob.glob(f"{path}-VELOC*.post.alyabin"):
            os.remove(file)
        for file in glob.glob(f"{path}-FIXNO*.post.alyabin"):
            os.remove(file)
        for file in glob.glob(f"{path}.ensi*"):
            os.remove(file)
            

# Get the downstream vector field after Alya simulation completed
def get_downstream_V_field(nF, c, a, v, p):
    # Modify case.ker.dat to set values for velocity angle and modulus and porosity 
    set_sim_vars(nF, c, a, v, p)
    # run the case with Alya in sequential mode
    run_alya(nF, c, False, HPC=True)
    # Create a class object that stores the Downstream witness mesh data
    #   from analysis.py
    wp = witnessPointsRect(nF, c, "DOWNW")
    # Create a file object in memory (for compatibility with functions)
    with io.StringIO() as f:
        write_wp_rect(wp, f)    # Write data inside 'wp' to f
        f.seek(0)               # Rewind memory file object
        lines = f.readlines()   # Read lines in file (downstream V field data)
    
    # Read text lines into numpy array
    dataDS = []  # Init Downstream data list
    for l in lines[1:]:
        dataDS.append([float(d) for d in l.split()])
    np_dataDS = np.array(dataDS)
    
    # Get list of coordinates
    x_list = sorted([*set(np_dataDS[:, [0]].flatten())])  # get a list of x coords set
    y_list = sorted([*set(np_dataDS[:, [1]].flatten())])  # and also from y

    # dimensions of the field
    n_x, n_y = (len(x_list), len(y_list))

    # 2 channels field initialization: vx, vy
    # and 1 channel for press
    v_field = np.zeros((n_x, n_y, 2))

    
    for d in np_dataDS:
        x, y = (d[0], d[1])  # coordinates
        vx, vy = (d[2], d[3])  # velocity components
        idx_x = x_list.index(x)  # x and y coordinates indices
        idx_y = y_list.index(y)
        # Velocity field component X
        v_field[idx_x, idx_y, 0] = vx
        # Velocity field component Y
        v_field[idx_x, idx_y, 1] = vy

    # clean output from simulation for the next one but keep necessary data
    #   for restarting the simulation with the previous values
    clean_outputs(nF, c)
    
    return v_field[:]